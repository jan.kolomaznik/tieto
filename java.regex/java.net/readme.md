[remark]:<class>(center, middle)
# Síťové operace

[remark]:<slide>(new)
## Síťová komunikace v Javě
* Podpora síťové komunikace je v Javě na velmi dobré úrovni.

* Třídy pro práci v síti najdeme v balíku `java.net`.

* Existuje ale i další podpůrné balíky jako `java.nio.channels`, `javax.net`, `javax.servlet`

[remark]:<slide>(new)
## Reprezentace IP adres
* IP adresa – unikátní číselná adresa každého komunikujícího uzlu sítě. 
  - Verze 4 (IPv4): 32bitové číslo (např. 192.168.1.2), 
  - Verze 6 (IPv6): 128bitové číslo (např. 2001:0718:1c01:0016:0214:22ff:fec9:0ca5).

* Třída `InetAddress` představuje obecnou IP adresu. 
  - Existují potomci pro obě verze – `Inet4Address` a `Inet6Address`. 
  - Pro většinu použití na verzi nezáleží, proto metody přijímají instance této obecnější třídy.

* Žádná z uvedených tříd nemá žádný veřejný konstruktor. 

[remark]:<slide>(new)
### Vytváření instací tříd `InetAddress`
Instance se vytvářejí statickými metodami a lze přitom použít některou z následujících variant:

* číselná reprezentace adresy – použije se metoda `static InetAddress getByAddress(byte[] addr)`, která vytvoří instanci adresy IPv4 (předá-li se pole 4 hodnot) nebo IPv6 (pro pole 16 hodnot).

* číselná + jmenná reprezentace – metoda static `InetAddress getByAddress(String host, byte[] addr)`. 
  Umožňuje vycházet z číselné reprezentace s tím, že se uvede i jmenná adresa, aniž by se přitom prováděl DNS dotaz. 
  Používání tohoto postupu je lépe se vyhnout.

* jmenná reprezentace – adresa se zadá obvyklým způsobem metodě `static InetAddress getByName(String host)`, ta provede DNS dotaz (přesněji řečeno, dotáže se systému, který může převody jmenných adres na číselné provádět různě) a vrátí instanci objektu Inet4Address nebo Inet6Address. 
  Místo jmenné adresy lze uvést i číselnou (v textovém tvaru), ta se pak přímo použije (jako při volání metody `getByAddress()`).

* získání lokální adresy – pokud potřebujeme lokální adresu, použijeme metodu `static InetAddress getLocalHost()`.

[remark]:<slide>(new)
#### Příklady instací tříd `InetAddress`
```java
byte[] ip4 = {195, 113, 194, 135};   // adresa IPv4
try {
   InetAddress ia = InetAddress.getByAddress(ip4);
   InetAddress ia2 = InetAddress.getByName("195.113.194.135");
   InetAddress ia3 = InetAddress.getByName("akela.mendelu.cz");
} catch (UnknownHostException e) {
   e.printStackTrace();
}
```

[remark]:<slide>(new)
### Třída `Socket`
* Každá aplikace komunikající po síti obsazuje nějaký port, který má číslo z intervalu 0–65535. 
  
* Použití portů s čísly do 1024 je omezené, volnější je použítí portů s čísly od 1025 výše.

* Port + IP adresa = socket. `Socket` je tedy jasně vymezený komunikační bod sítě (adresa počítače + příslušná aplikace).

* Třída InetSocketAddress sdružuje adresu a číslo portu do jediného objektu. Instanci této třídy již vytváříme obvyklým způsobem, tj. voláním konstruktoru.


```java
try {
   InetAddress ia = InetAddress.getByName("akela.mendelu.cz");
   InetSocketAddress isa = new InetSocketAddress(ia, 80);
   InetSocketAddress isa2 = new InetSocketAddress("akela.mendelu.cz", 80);
} catch (UnknownHostException e) {
   e.printStackTrace();
}
```
