package regex;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Net {

    public static void main(String[] args) {
        try {
            URL url = new URL("http://www.pef.mendelu.cz/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            System.out.println("Response code: " + con.getResponseCode());
            System.out.println("Content type: " + con.getContentType());

            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String s = br.readLine();
            while (s != null) {
                System.out.println(s);
                s = br.readLine();
            }

            con.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
