package regex;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    public static void main(String[] args) {
        Pattern pat = Pattern.compile(".*,(.*)=.*");
        try {
            BufferedReader vstup = new BufferedReader(
                    new FileReader("zahhada.sys"));
            String radek;
            while ((radek = vstup.readLine()) != null ) {
                Matcher matcher = pat.matcher(radek);
                if (matcher.matches()) {
                    System.out.println(matcher.group(1));
                }
            }
            vstup.close();
        } catch (IOException e) {
            System.out.println ("Chyba na vstupu souboru ");
        }
    }
}
