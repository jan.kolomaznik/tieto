package regex;

import java.io.*;
import java.util.regex.*;
public class Grep {
    static Pattern pat;

    static void grep (String soubor) {
        try {
            BufferedReader vstup = new BufferedReader(
                    new FileReader (soubor));
            String radek;
            while ((radek = vstup.readLine()) != null ){
                if (pat.matcher(radek).find()){
                    System.out.println(soubor+": "+radek);
                }
            }
            vstup.close();
        } catch (IOException e) {
            System.out.println ("Chyba na vstupu souboru "+soubor);
        }
    }

    public static void main (String [] args) {
        if (args.length < 2 ) {
            System.out.println("grep VZOR SOUBOR ...");
            System.exit(1);
        }
        try {
            pat = Pattern.compile(args[0]);
        } catch (PatternSyntaxException pe) {
            System.out.println(pe.getMessage());
            System.exit(1);
        }
        for (int i=1; i< args.length; i++) {
            grep(args[i]);
        }
    }
}
