package regex;

import java.io.*;
import java.net.Socket;

public class TCPClient {

    public static void main(String[] args) throws IOException {
        Socket sock = new Socket("akela.mendelu.cz", 80); // připojení
        BufferedReader br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
        bw.write("GET / HTTP/1.0\\r\\n\\r\\n");  // zapíšeme předem připravený požadavek
        bw.flush();         // vyprázdnění (tzn. odeslání) bufferu
        String line = br.readLine();
// dokud jsou data, opakuj
        while (line != null) {
            System.out.println(line);  // platná data vypisuj
            line = br.readLine();
        }
        sock.close(); // zavření socketu
    }
}
