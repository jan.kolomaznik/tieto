package regex;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class Split {

    static class Counter {
        int pocet = 1;

        public String toString () {
            return Integer.toString(pocet);
        }
    }

        static Pattern pat = Pattern.compile("[,=]");

        public static void main (String [] args) {
            Map seznam = new HashMap();
                String soubor = "zahhada.sys";
                try {
                    BufferedReader vstup = new BufferedReader(
                            new FileReader (soubor));
                    String radek;
                    while ((radek = vstup.readLine()) != null ) {
                        String [] vysl = pat.split(radek);
                        if (vysl.length != 3) {
                            continue;
                        }
                        String bootFile = vysl[2].replaceAll(" +","");
                        if (seznam.containsKey(bootFile)) {
                            ((Counter)seznam.get(bootFile)).pocet++;
                        } else {
                            seznam.put(bootFile,new Counter());
                        }
                    }
                    vstup.close();
                } catch (IOException e) {
                    System.out.println ("Chyba na vstupu souboru "+soubor);
                }

            System.out.println(seznam);

    }
}
