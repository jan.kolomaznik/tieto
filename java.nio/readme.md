[remark]:<class>(center, middle)
# New I/O

[remark]:<slide>(new)
## Uvod
* New I/O knihovna se skládá z pěti balíků: `java.nio`,` java.nio.channels`, `java.nio.charset`, `java.nio.chan­nels.spi` a `java.nio.char­set.spi`. 

* Nás budou zajímat pouze první tři, další dva nebudeme vůbec využívat. 

* Pokud máte zájem dozvědět se o nich něco, nahlédněte do dokumentace k Java API.

* V `java.nio` jsou umístěny buffery, které si ještě dnes popíšeme. 

* Kanály a selektory jsou obsaženy v balíku `java.nio.channels`. 

* Nakonec `java.nio.charset` poskytuje API pro práci se znakovými sadami.

* Ani již známé knihovny `java.ne`t a `java.io` nezůstanou stranou. NIO je nad nimi vystavěno, tudíž i je si budeme muset naimportovat.

[remark]:<slide>(new)
## Buffery
* Když jsme potřebovali přenést nějaká data do soketu nebo naopak z něj, vystačili jsme si s vyrovnávací pamětí implemetovanou jako pole bajtů. 

* NIO však přišlo s chytřejším a především rychlejším řešením než javová pole. 

* Pro každý primitivní datový typ kromě `boolean` nabízí třídu bufferu. 

* Ta umožňuje takové operace, jakými jsou například přetáčení, nastavení značky atd. 

* Navíc bajtový buffer lze alokovat jako nativní datovou strukturu.

[remark]:<slide>(new)
### Třídy
* Jak již bylo řečeno, buffery se nacházejí v balíčku `java.nio` a existují pro všechny primitivní typy kromě `boolean`. 

* Tedy jen krátce, jsou to třídy `ByteBuffer`, `CharBuffer`, `DoubleBuffer`, `FloatBuffer`, `IntBuffer`, `LongBuffer`, `ShortBuffer`.

* Zajímavostí je třída `MappedByteBuffer` vhodná pro práci s velkými soubory. 

* Všechny buffery sjednocuje společná abstraktní třída `Buffer`.

[remark]:<slide>(new)
### Alokace bufferů
* Bajtové buffery je možné alokovat jako přímé nebo nepřímé. 

* **Přímé** nabízejí vyšší výkon, protože data se ukládají do nativních datových struktur. 
  - Hodí se zejména pro práci se soubory a sokety, budeme je tedy hojně využívat. 
  - Přímý bajtový buffer vytvoříme metodou `allocateDirect(int kapacita)`. 
  - A jedno důležité upozornění: bajtový buffer neznamená, že se do něj dají ukládat pouze bajty. 
  
* Pokud chceme použít jiný buffer než bajtový, jedinou možností je **alokovat buffer nepřímo**. 
  - K tomu slouží metoda allocate(int kapacita). 
  - Nepřímé buffery jsou vhodné pro práci s daty pouze v rámci JVM, nebo musíme-li je často alokovat a mazat.

[remark]:<slide>(new)
### Práce s buffery
* Každý buffer má čtyři důležité hodnoty: kapacitu, pozici, limit a značku. 

* Práci s těmito hodnotami umožňují metody z abstraktní třídy `Buffer`.

#### Kapacita, pozice, limit a značka
* **Kapacita** čili velikost vyrovnávací paměti je konstantní hodnota, kterou jsme zadali jako parametr metody `allocate()` nebo `allocateDirect()`.
  - Zjistíme ji metodou `capacity()`.

* **Limit** je značka určená pro čtení a zápis do bufferu. 
  - Označuje index prvního prvku, který ještě nelze přečíst, nebo pozici, na kterou ještě nelze zapisovat (místo není volné). 
  - Aktuální limit vrací metoda `limit()`, změníme ho pomocí stejnojmenné metody s jedním celočíselným parametrem – `limit(int novýLimit)`.

[remark]:<slide>(new)
* **Pozice** určuje index aktuálního elementu, na kterém bude probíhat čtení nebo zápis. 
  - Zjišťuje a nastavuje se obdobně jako limit, tentokrát však přetíženou metodou `position()`.
  
* **Značka** je index prvku, který si můžeme označit metodou `mark()` a vrátit se na jeho pozici voláním `reset()`.

#### Metody
* **clear()** tato metoda „vymaže“ obsah bufferu nastavením nulové pozice a limitu na kapacitu. 
  Zároveň zruší značku. 
  Buffer je pak připraven k novému použití.

* **flip()** Tuto metodu použijeme, když je buffer plný (nebo obsahuje požadované množství dat). 
  Nastaví totiž limit na aktuální pozici, kterou následně resetuje na nulu. 
  Tím máme zaručeno, že se nepřečte více prvků než chceme.

* **rewind()** Přetočí buffer. 
  Vynuluje pozici a zruší značku, takže je možné z bufferu opět přečíst stejná data.

[remark]:<slide>(new)
## Kanály
* Kanály reprezentují různé síťové prostředky, soubory, roury a pro nás nejdůležitější – síťové sokety. 

* Společně s buffery nabízejí stejnou funkčnost jako třídy `InputStream`, `OutputStream`, `Writer` a `Reader`. 

* Všechny NIO kanály se nacházejí v balíku `java.nio.channels`.

* Kanály jsou odvozeny z mnoha různých rozhraní a abstraktních tříd. 

* Od Channel se dvěmi metodami `isOpen()` a `close()` až po třídu `SelectableChannel`, jenž umožňuje poměrně pokročilé operace, jakou je třeba zaregistrování selektoru.

* Balík java.nio.channels obsahuje dva kanály pro rouru – `Pipe.SourceChannel` a `Pipe.SinkChannel`, `FileChannel` pro práci se soubory a tři kanály pro práci se sítí – `DatagramChannel` pro UDP, `SocketChannel` a `ServerSocketChannel` pro TCP/IP přenos.