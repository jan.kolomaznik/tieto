package jm.test.neco

import jm.test.spock.Car
import jm.test.spock.CarRepository
import jm.test.spock.ImplCarService
import jm.test.spock.RentService
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

class ImplCarServiceTest extends Specification {

    private ImplCarService carService;

    private CarRepository carRepository;

    private RentService rentService;

    public void setup() {
        carService = new ImplCarService();
        carService.carRepository = carRepository = Mock(CarRepository)
        carService.rentService = rentService = Mock(RentService)
    }

    def "CreateCar"() {
        when:
        Car car = carService.createCar("Beriska");

        then:
        car != null;
        1 * carRepository.save(_) >> new Car()
    }

    @Unroll
    def "createId #name -> #result"() {
        expect:
        carService.createId(name) == result

        where:
        name  | result
        "AA"  | 2
        "ABC" | 4

    }

    @Ignore
    def "test get car back"() {
        setup:
        Car car = new Car(id: 1, rent: false);

        when:
        boolean result = carService.putCarBack(car);

        then:
        result == false
    }

    def "private plus metod"() {
        when:
        def r = carService.plus(1, 2)

        then:
        r == 4
    }

    def "Rent car test"() {
        given: """
 
                cmv xc;lv
                xcvx';c
        """
        Car car = new Car(id: 1, rent: false);
        Car car2 = new Car(id: 2, rent: false);

        when:
        carService.rentCar(car, "Pepa");

        then:
        1 * rentService.canRent(_, _) >> true
        car.isRent() == true
    }
}
