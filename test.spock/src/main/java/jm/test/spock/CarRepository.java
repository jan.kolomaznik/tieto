package jm.test.spock;

import java.util.List;
import java.util.Optional;

public interface CarRepository {

    Car save(Car car);

    Car findById(long carId);

    List<Car> findAllCars();
}
