package jm.test.spock;

public class ImplCarService implements CarService {

    private CarRepository carRepository;

    private RentService rentService;

    @Override
    public Car createCar(String name) {
        Car car = new Car();
        car.setName(name);
        car = carRepository.save(car);
        return car;
    }

    @Override
    public Car rentCar(Car car, String user) {
        if (rentService.canRent(car, user)) {
            car.setRent(true);
            return carRepository.save(car);
        } else {
            throw new UnsupportedOperationException("Car cant by rent.");
        }
    }

    @Override
    public int createId(String name) {
        return name.length();
    }

    private int plus(int a, int b) {
        return a + b;
    }
}
