package jm.test.spock;

public interface RentService {

    boolean canRent(Car car, String user);
}
