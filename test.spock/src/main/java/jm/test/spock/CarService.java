package jm.test.spock;

public interface CarService {

    Car createCar(String name);

    Car rentCar(Car car, String user);

    int createId(String name);
}
