[remark]:<class>(center, middle)
# Přehlednávrhových vzorů:  
----
*Zpřehlednění (zjednodušení) kódu*

[remark]:<slide>(new)
## Decorator
Přidává další funkcionalitu k objektu tak, že objekt „zabalí“ do jiného objektu, který má na starosti pouze přidanou funkcionalitu, a zbytek požadavků deleguje na „zabalený“ objekt. 

Tím umožňuje přidávat funkčnost dynamicky a zavádí flexibilní alternativu k dědění.

[remark]:<slide>(wait)
![](media/Decorator.svg)

[remark]:<slide>(new)
### Příklad dekorátoru 
![](media/Decorator_example.svg)

[remark]:<slide>(new)
### Příklad implementace
Create an interface.

```java
public interface Shape {
   void draw();
}
```

[remark]:<slide>(wait)
Create concrete classes implementing the same interface.

```java
public class Rectangle implements Shape {

   @Override
   public void draw() {
      System.out.println("Shape: Rectangle");
   }
}
```

```java
public class Circle implements Shape {

   @Override
   public void draw() {
      System.out.println("Shape: Circle");
   }
}
```

[remark]:<slide>(new)
Create abstract decorator class implementing the Shape interface.

```java
public abstract class ShapeDecorator implements Shape {
   protected Shape decoratedShape;

   public ShapeDecorator(Shape decoratedShape){
      this.decoratedShape = decoratedShape;
   }

   public void draw(){
      decoratedShape.draw();
   }	
}
```

[remark]:<slide>(wait)
Create concrete decorator class extending the ShapeDecorator class.

```java
public class RedShapeDecorator extends ShapeDecorator {

   public RedShapeDecorator(Shape decoratedShape) {
      super(decoratedShape);		
   }

   @Override
   public void draw() {
      decoratedShape.draw();	       
      setRedBorder(decoratedShape);
   }

   private void setRedBorder(Shape decoratedShape){
      System.out.println("Border Color: Red");
   }
}
```

[remark]:<slide>(new)
Use the RedShapeDecorator to decorate Shape objects.

```java
public class DecoratorPatternDemo {
   public static void main(String[] args) {

      Shape circle = new Circle();

      Shape redCircle = new RedShapeDecorator(new Circle());

      Shape redRectangle = new RedShapeDecorator(new Rectangle());
      System.out.println("Circle with normal border");
      circle.draw();

      System.out.println("\nCircle of red border");
      redCircle.draw();

      System.out.println("\nRectangle of red border");
      redRectangle.draw();
   }
}
```

[remark]:<slide>(new)
## Chain of Responsibility
Existuje několik instancí různých tříd, které mohou potenciálně zpracovat požadavek. 

[remark]:<slide>(wait)
![](media/Chain_of_responsibility_motivation.svg)

[remark]:<slide>(wait)
Klient jej odesílá bez přesného určení konkrétní instance. 

![](media/Chain_of_responsibility.svg)

[remark]:<slide>(new)
### Příklad implementace
Create an abstract logger class.

```java
public abstract class AbstractLogger {
   public static int INFO = 1;
   public static int DEBUG = 2;
   public static int ERROR = 3;

   protected int level;

   //next element in chain or responsibility
   protected AbstractLogger nextLogger;

   public void setNextLogger(AbstractLogger nextLogger){
      this.nextLogger = nextLogger;
   }

   public void logMessage(int level, String message){
      if(this.level <= level){
         write(message);
      }
      if(nextLogger !=null){
         nextLogger.logMessage(level, message);
      }
   }

   abstract protected void write(String message);
	
}
```

[remark]:<slide>(new)
Create concrete classes extending the logger.

```java
public class ConsoleLogger extends AbstractLogger {

   public ConsoleLogger(int level){
      this.level = level;
   }

   @Override
   protected void write(String message) {		
      System.out.println("Standard Console::Logger: " + message);
   }
}
```

```java
public class ErrorLogger extends AbstractLogger {

   public ErrorLogger(int level){
      this.level = level;
   }

   @Override
   protected void write(String message) {		
      System.out.println("Error Console::Logger: " + message);
   }
}
```

```java
public class FileLogger extends AbstractLogger {

   public FileLogger(int level){
      this.level = level;
   }

   @Override
   protected void write(String message) {		
      System.out.println("File::Logger: " + message);
   }
}
```

[remark]:<slide>(new)
Create different types of loggers. Assign them error levels and set next logger in each logger. Next logger in each logger represents the part of the chain.

```java
public class ChainPatternDemo {
	
   private static AbstractLogger getChainOfLoggers(){

      AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
      AbstractLogger fileLogger = new FileLogger(AbstractLogger.DEBUG);
      AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);

      errorLogger.setNextLogger(fileLogger);
      fileLogger.setNextLogger(consoleLogger);

      return errorLogger;	
   }

   public static void main(String[] args) {
      AbstractLogger loggerChain = getChainOfLoggers();

      loggerChain.logMessage(AbstractLogger.INFO, 
         "This is an information.");

      loggerChain.logMessage(AbstractLogger.DEBUG, 
         "This is an debug level information.");

      loggerChain.logMessage(AbstractLogger.ERROR, 
         "This is an error information.");
   }
}
```

[remark]:<slide>(new)
## Observer
Definování závislosti jednoho objektu k více objektům. 
  
Umožnění šíření události, která nastala v jednom objektu, ke všem závislým objektům.

[remark]:<slide>(wait)
![](media/Observer.svg)

[remark]:<slide>(new)
### Příklad implementace
Create Subject class.

```java
import java.util.ArrayList;
import java.util.List;

public class Subject {
	
   private List<Observer> observers = new ArrayList<Observer>();
   private int state;

   public int getState() {
      return state;
   }

   public void setState(int state) {
      this.state = state;
      notifyAllObservers();
   }

   public void attach(Observer observer){
      observers.add(observer);		
   }

   public void notifyAllObservers(){
      for (Observer observer : observers) {
         observer.update();
      }
   } 	
}
```

[remark]:<slide>(new)
Create Observer class.

```java
public abstract class Observer {
   protected Subject subject;
   public abstract void update();
}
```

[remark]:<slide>(wait)
Create concrete observer classes

```java
public class BinaryObserver extends Observer{

   public BinaryObserver(Subject subject){
      this.subject = subject;
      this.subject.attach(this);
   }

   @Override
   public void update() {
      System.out.println( "Binary String: " + 
            Integer.toBinaryString( subject.getState() ) ); 
   }
}
```

[remark]:<slide>(new)
```java
public class OctalObserver extends Observer{

   public OctalObserver(Subject subject){
      this.subject = subject;
      this.subject.attach(this);
   }

   @Override
   public void update() {
     System.out.println( "Octal String: " + 
            Integer.toOctalString( subject.getState() ) ); 
   }
}
```

```java
public class HexaObserver extends Observer{

   public HexaObserver(Subject subject){
      this.subject = subject;
      this.subject.attach(this);
   }

   @Override
   public void update() {
      System.out.println( "Hex String: " + 
            Integer.toHexString( subject.getState() ).toUpperCase() ); 
   }
}
```

[remark]:<slide>(new)
Use Subject and concrete observer objects.

```java
public class ObserverPatternDemo {
   public static void main(String[] args) {
      Subject subject = new Subject();

      new HexaObserver(subject);
      new OctalObserver(subject);
      new BinaryObserver(subject);

      System.out.println("First state change: 15");	
      subject.setState(15);
      System.out.println("Second state change: 10");	
      subject.setState(10);
   }
}
```

[remark]:<slide>(new)
## Mediator
Jakým způsobem zajistit komunikaci mezi dvěma komponentami programu, aniž by byly v přímé interakci a tím musely přesně znát poskytované metody.

[remark]:<slide>(wait)
![](media/Mediator_1.svg)

[remark]:<slide>(wait)
![](media/Mediator_2.svg)

[remark]:<slide>(new)
### Příklad implementace
Create mediator class.

```java
import java.util.Date;

public class ChatRoom {
   public static void showMessage(User user, String message){
      System.out.println(new Date().toString() + " [" + 
                         user.getName() + "] : " + message);
   }
}
```

[remark]:<slide>(new)
Create user class

```java
public class User {
   private String name;

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public User(String name){
      this.name  = name;
   }

   public void sendMessage(String message){
      ChatRoom.showMessage(this,message);
   }
}
```

[remark]:<slide>(new)
Use the User object to show communications between them.

```java
public class MediatorPatternDemo {
   public static void main(String[] args) {
      User robert = new User("Robert");
      User john = new User("John");

      robert.sendMessage("Hi! John!");
      john.sendMessage("Hello! Robert!");
   }
}
```
