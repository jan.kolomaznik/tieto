package cz.ictpro.demo;

import java.io.*;

public class TryCachDemo {

    public static void main(String[] args) {

        try (Writer writer = new StringWriter()) {
            writer.write("Pokus");
        } catch (IOException | NullPointerException | UnsupportedOperationException e) {
            e.printStackTrace();
        }

        Writer writer2 = null;
        Exception main = null;
        try  {
            writer2 = new StringWriter();
            writer2.write("Pokus");
        } catch (IOException e) {
            main = e;
            throw new UnsupportedOperationException("neco", main);
        } finally {
            if (writer2 != null) {
                try {
                    writer2.close();
                } catch (IOException e) {
                    main.addSuppressed(e);
                }
            }
        }

    }
}
