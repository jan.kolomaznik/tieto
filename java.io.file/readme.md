[remark]:<class>(center, middle)
# Práce se soubory

[remark]:<slide>(new)
## Úvod
* Java je koncipována jako platformově nezávislá, takže se musí vypořádat s různými souborovými systémy. 

* Zajímají nás především dvě nejrozšířenější skupiny souborových systémů:
  - **Systémy unixového typu** – je jich celá řada (UFS, Ext4, ReiserFS atd.), pro všechny z nich je typické, že celý systém tvoří jediný adresářový strom, jako oddělovač jednotlivých úrovní se používá běžné (dopředné) lomítko, rozlišují se velká a malá písmena, téměř všechny znaky ASCII jsou povolené pro použití v názvech souborů a pro ukládání názvů lze obecně používat různé kódové stránky.
  - **Systémy firmy Microsoft** (FAT a NTFS) – rozlišují se jednotlivá logická zařízení („písmena disků“), oddělovačem je zpětné lomítko, velikost písmen se nerozlišuje, okruh platných znaků je poměrně malý (i když to některé systémové funkce řádně nekontrolují a lze tak na disk propašovat i soubory s neplatnými názvy, které pak lze těžko odstranit), kódování se liší podle jednotlivých systémů (NTFS používá Unicode, FAT potom staré kódové stránky, např. pro češtinu CP 852).

[remark]:<slide>(new)
### Třída `File` 
Základní třídou pro práci se soubory je třída `File` z balíku `java.io`. 
* Nepředstavuje přímo konkrétní soubor, nýbrž tzv. abstraktní cestu (tedy obecně jakoukoli cestu identifikující nějaký soubor). 

* Může odkazovat na platný soubor, ale také nemusí. 

* Je jakousi sofistikovanější náhradou zápisu cesty k souboru v podobě řetězce (objektu) typu `String`. 

* Řada metod, které vyžadují jako svůj argument název souboru (např. konstruktor `FileInputStream`), jsou schopny přijmout jak odkaz na instanci třídy `String`, tak i `File`, což je přenositelnější a robustnější řešení, protože můžeme již předem zjistit o souboru daného názvu nějaké informace nebo provést se souborem potřebné operace.

[remark]:<slide>(new)
### Třída `File` 
Objekt File může představovat jak soubory (běžné, ale i speciální, třeba soubory zařízení), tak adresáře. 
* Cesta může být absolutní i relativní, může být dokonce i prázdná. 

* Abstraktní cesta se vždy skládá z prefixu (např. označení kořenového adresáře; u relativních cest prefix samozřejmě chybí) a z posloupnosti názvů jednotlivých adresářových úrovní (samozřejmě včetně případného názvu souboru na konci) oddělených separátorem. 

* Oddělovač názvů v cestě je ve třídě File určen hodnotou statické konstanty separatorChar (typu char), resp. separator (typu String). 

* Na unixových systémech je oddělovačem samozřejmě dopředné lomítko, na microsoftích systémech lomítko obrácené.

[remark]:<slide>(new)
### Operace s instancí třídy File
Vytvoření instance třídy File:
* parametrem konstruktoru je buď String s relativní nebo absolutní cestou k souboru/adresáři
  
  `String path = "C:" + File.separator + "java" + File.separator + "projects" + File.separator + "cviceni9" + File.separator + "data.dat";)`

* instance třídy URI

  `URI u = new URI("http://old.mendelu.cz/~petrj/java/ppt/data.txt"))`

[remark]:<slide>(new)
### Práce s cetsou k souboru
* Objekt File je abstraktní cestou k souboru, se kterou můžeme pracovat a získávat různé její varianty. 

* Lze získat celou cestu v různých podobách, části cesty a některé další verze. 

[remark]:<slide>(wait)
#### `String getPath()`
* vrátí abstraktní cestu v podobě, jak byla zadána při vytváření objektu (tedy absolutní zůstane absolutní atd.). 
* Stejný efekt má i metoda `String toString()`.

[remark]:<slide>(wait)
#### `String getAbsolutePath()` 
* vrátí cestu převedenou do absolutního tvaru. 
* Mechanismus případného převodu z relativní cesty na absolutní je platformově závislý, většinou se ale jako báze použije domovský adresář uživatele.

[remark]:<slide>(new)
#### `String getCanonicalPath()` 
* vrací kanonický tvar cesty. V praxi to znamená, že se pokusí cestu maximálně vyhodnotit, zpracovat. 
* Odstraní všechny označení stejného nebo nadřazeného adresáře (tečku a dvě tečky), zpracuje symbolické odkazy atd. 
* Chování je silně platformově závislé a liší se podle toho, zda cesta (nebo její části) existuje či nikoli.

[remark]:<slide>(wait)
#### `String getName()` 
* získá z cesty pouhý název souboru (bez adresářové cesty).

[remark]:<slide>(wait)
#### `String getParent()` 
* vrací rodičovský adresář souboru. 
* Vychází se pouze z cesty, soubor ani rodičovský adresář nemusí existovat.

[remark]:<slide>(new)
#### `boolean isAbsolute()` 
* zjistí, zda je cesta absolutní.

[remark]:<slide>(wait)
#### `int compareTo(File pathname)` 
* lexikograficky porovná tuto abstraktní cestu s jinou (ani jedna nemusí existovat). 
* Porovnávání je platformově závislé, podle systému se použije rozlišení malých/velkých písmen. 
* Podobně pracuje metoda boolean equals(Object obj), která pouze zjišťuje, zda jsou abstraktní cesty totožné.

[remark]:<slide>(new)
### Adresářové informace
* Předchozí metody se týkaly všech souborů bez rozdílu, tedy včetně adresářů. 

* Pro adresáře samotné máme k dispozici speciální sadu metod, které využijeme pro přístup k souborům v těchto adresářích

[remark]:<slide>(wait)
#### `String[] list()` 
* nejjednodušší varianta. 
* Vrátí pole obsahující seznam všech souborů v daném adresáři – položky tohoto pole budou textové řetězce s názvy souborů. 
* Pořadí souborů není definováno, může být libovolné.

[remark]:<slide>(wait)
#### `File[] listFiles()`
* dělá přesně totéž co `list()`, ale místo pole textových řetězců vrací pole objektů `File`, tedy abstraktních cest.

[remark]:<slide>(new)
#### `String[] list(FilenameFilter filter)`
* modifikace metody list() s tím, že předem vybíráme jen některé soubory. 
* Které to budou, to určí implementace rozhraní `FilenameFilter` (viz níže).

[remark]:<slide>(wait)
#### `File[] listFiles(FilenameFilter f)`
* modifikace metody listFiles() s tím, že opět předem vybíráme jen některé soubory.

[remark]:<slide>(wait)
#### `File[] listFiles(FileFilter f)`
* další modifikace, ale s jiným typem filtru.

[remark]:<slide>(wait)
#### `static File[] listRoots()`
* v souborových systémech unixovského typu je hierarchie přísně stromová, vždy máme jediný kořen. 
* V jiných systémech to ale platit nemusí (a také neplatí), proto je třeba mít možnost dostat se ke všem dostupným kořenům – a to zajišťuje právě tato statická metoda. 
* Vrací pole všech kořenů adresářových stromů, které jsou v danou chvíli k dispozici.

[remark]:<slide>(new)
### Filtrace
* Při filtraci musíme implementovat rozhraní `FilenameFilter` nebo `FileFilter`. 

* V obou případech musíme implementovat metodu `boolean accept()`. 

* Pokud budeme implementaci potřebovat jen v jednom jediném případě, s výhodou využijeme možnosti vytvořit anonymní třídu přímo na daném místě.

```java
File f = new File("/home/username/docs");       // vybereme adresář

String[] list = f.list(new FilenameFilter() {
      boolean accept(File dir, String name) {
        return name.endsWith(".pdf");           // jen názvy *.pdf
   }
});

java.util.Arrays.sort(list);    // abecední seřazení

for (int i = 0; i < list.length; i++) {
     System.out.println(list[i]);
}
```

[remark]:<slide>(new)
### Manipulační operace:
#### `boolean renameTo(File f2)` 
* metoda přejmenuje soubor podle zadání. 
* Jako parametr se zadává jiná instance objektu File. 
* Jelikož je původní instance File neměnná, i po úspěšném přejmenování souboru zůstane tak, jak je (bude obsahovat původní cestu k souboru). 
* Naopak nové jméno souboru bude odpovídat zadané instanci f2, o čemž se můžeme přesvědčit tak, že zavoláme metodu boolean exists(). 
* Chování je silně platformově závislé, nemůžeme spoléhat, že metoda bude dělat vždy to, co dělala na některé platformě. 
* Návratovou hodnotu je třeba vždy testovat.

[remark]:<slide>(wait)
#### `boolean delete()`
* pokusí se smazat soubor. 
* Pokud to jde, smaže ho. 
* Neprázdné adresáře mazat nelze, musíme je nejdříve vyprázdnit.

[remark]:<slide>(new)
#### `void deleteOnExit()` 
* zajímavá metoda, naplánuje smazání souboru při ukončování programu. 
* Zafunguje pouze při regulérním ukončení programu, tedy ne při násilném (na Linuxu signálem SIGKILL) nebo při zavolání metody System.halt(). 
* Metoda se používá pro automatické mazání dočasných souborů. Pozor! Naplánované smazání už nejde zrušit!

[remark]:<slide>(wait)
#### `boolean createNewFile()`
* vytvoří nový prázdný soubor.

[remark]:<slide>(wait)
#### `static File createTempFile(String prefix, String suffix)`
* vytvoří nový prázdný soubor v adresáři pro dočasné soubory.

[remark]:<slide>(wait)
#### `boolean mkdir(), boolean mkdirs()`
* dvojice metod pro vytváření adresářů. 
* Liší se pouze tím, že ta první vytvoří pouze ten jediný adresář, na který odkazuje instance File, kdežto ta druhá vytvoří, pokud je třeba, i všechny nadřazené adresáře.

[remark]:<slide>(new)
#### `boolean setLastModified(long time)`
* změní časový údaj o poslední změně souboru. (Někdy se to může hodit.)

[remark]:<slide>(wait)
#### `boolean setReadOnly()`
* nastaví, že soubor bude pouze ke čtení.

[remark]:<slide>(new)
### Operace pro zjištění informací:
#### `long getTotalSpace()`
* zjistí celkovou kapacitu (počet bajtů) diskového svazku, na kterém se nachází soubor.

[remark]:<slide>(wait)
#### `long getUsableSpace()`
* zjistí volnou kapacitu (počet bajtů) diskového svazku, na kterém se nachází soubor.

[remark]:<slide>(wait)
#### `long length()` 
* zjistí velikost souboru (počet bajtů).

[remark]:<slide>(wait)
#### `long lastModified()` 
* zjistí datum a čas poslední úpravy souboru.