[remark]:<class>(center, middle)
# Spring Boot
*Aspektově orientovane programování*

[remark]:<slide>(new)
## Úvod
Aspektově orientované programování (dále jen AOP) je technika, která vznikla přibližně před 10 lety.

Je nadstavbou objektově orientovaného programování a umožňuje snáze naprogramovat některé často se opakující činnosti.

Není alternativou k OOP, ale naopak komplementem.

OOP se k některým potřebám příliš nehodí a proto je nad ním AOP, které tyto potřeby dokáže snadno naplnit.

AOP se začalo hodně používat zejména od r. 2004.

Cílem AOP je nahradit v kódu opakující se činnosti, nejsnadněji to je možné ukázat na logování.

V případě, že chceme logovat například předa po provedení některých metod, musíme před každou a po každé zapsat kód pro logování a není snadné ho potom měnit.

[remark]:<slide>(new)
### Základní pojmy
Ještě než budeme pokračovat, je ale potřeba definovat několik termínů, které budeme dále používat:

- **Joinpoint** – místo, kam je možné vložit do kódu další logiku pomocí AOP, patří sem jako nejčastěji používané volání metody, nebo spuštění metody, či inicializace třídy apod.
- **Advice** – kód, který je spuštěn v určitém joinpointu, existuje více druhů advice, např. before (spouští se před joinpointem) nebo after (za joinpointem)
- **Pointcut** – je souborem joinpointů, pro které je spouštěna stejná advice, díky granularitě umožňuje snadnou kontrolu nad spouštěním AOP kódu
- **Aspect** – je kombinací advice a pointcut, určuje tedy logiku, která je do aplikace vložena a místo, kde bude spuštěna
- **Weaving** – proces vkládání aspektů do aplikace
- **Target** – cíl, objekt, jehož chování je ovlivněno nějakým AOP kódem

[remark]:<slide>(new)
## Typy AOP
Existují dva typy AOP, statické a dynamické.

Statické AOP poskytuje například AspectJ, dynamické např.

Spring Framework.

Implementací AOP existuje velké množství, nejstarší z nich je právě AspectJ.

[remark]:<slide>(new)
### Statické AOP
Při statickém zpracování probíhá weaving při buildu aplikace, přibývá zde další krok.

Díky tomu je statické AOP rychlejší než dynamické, kód AOP ale nelze měnit za běhu aplikace, není možné přidávat či odebírat funkčnost za běhu.

Jakákoliv změna v AOP tedy nutí k opětovné kompilaci celé aplikace.

Příkladem využití statického AOP je právě AspectJ.

[remark]:<slide>(new)
### Dynamické AOP
U dynamických implementací AOP probíhá weaving proces za běhu aplikace.

Toho je dosaženo u různých implementací pomocí různých technik, nejčastěji se ale používají proxy pro každý objekt, který využívá aspekty.

Nevýhodou je nižší výkon oproti statické implementaci, výhodou možnost měnit AOP kód úplně nezávisle na aplikaci, změny v AOP tedy nutně neznamenají potřebu opětovné kompilace celé aplikace.

Dynamické AOP využívá například Spring Framework.

[remark]:<slide>(new)
## Příklad využití AOP
Ukážeme si nyní na jednoduchém příkladu, v jakém případě je efektivní AOP využít.

Příklad bude uveden v programovacím jazyku Java za využití jak AspectJ, tak Spring AOP, abychom prezentovali rozdíly mezi implementacemi statickými a dynamickými.

Poznámka: Tento příklad je pouze pro ilustrační účely, stejného výsledku bychom v tomto případě mohli docílit pomocí mnohem jednodušších metod.

 

Základem je třída, která na obrazovku vypíše text „Ahoj Karle!“.

```java 
package aopPrikladSpring;

public class PisatelZpravy {

      public void napisZpravu() {
            System.out.println("Ahoj Karle!");
      }
}
```

[remark]:<slide>(new)
### Cíl úpravy pomocí AOP
Nyní budeme chtít, aby při každém vypsání zprávy program uživatele pozdravil „Dobrý den“, pak vypsal zprávu a na konec se rozloučil „Nashledanou“.

Znamená to, že budeme používat Advice, která bude vypisovat kód před spuštěním metody napisZpravu a po jejím spuštění, joinpointem je zde tedy spuštění metody.

První třída je stejná jak pro AspectJ tak pro Spring AOP, jen náleží do jiné package.

[remark]:<slide>(wait)
#### Příklady pointcutu
- **`execution(public * *(..))`** - zavolání libovolné public metody
- **`execution(* set*(..))`** - zavolání libovolné metody se jménem začínajícím na set
- **`execution(* com.xyz.service.AccountService.*(..))`** - zavolání libovolné metody definované v interface AccountService
- **`execution(* com.xyz.service..*.*(..))`** - zavolání libovolné metody v package com.xyz.service nebo v podřízeném package
- **`@annotation(org.springframework.transaction.annotation.Transactional)`** - zavolání metody s anotací @Transactional
- **`bean(tradeService)`** - zavolání libovolné metody ve Spring beanu tradeService

[remark]:<slide>(new)
### Spring AOP

```java
package aopPrikladSpring;

@Aspect
@Component
public class VylepsovacZpravy implements MethodInterceptor {

    @Pointcut("execution(public napisZpravu(..))")
    public void zavolejVypisZpravy(){}

    @Before("aopPrikladSpring.VylepsovacZpravy.zavolejVypisZpravy()")
    public void before(JoinPoint joinPoint) {
            System.out.println("Dobrý den");
    }
      
    @After("aopPrikladSpring.VylepsovacZpravy.zavolejVypisZpravy()")
    public void after(JoinPoint joinPoint) {
        System.out.println("Nashledanou"); 
    }
}

```

[remark]:<slide>(new)
### AspectJ

```aspectj
package aopPrikladAspectJ;

public aspect VylepsovacZpravy {

      pointcut zavolejVypisZpravy() : call(public void PisatelZpravy.napisZpravu(..));
      
      before() : zavolejVypisZpravy() {
            System.out.println("Dobrý den");
      }
     
      after () : zavolejVypisZpravy() {
            System.out.println("Nashledanou");
      }
}
```

Tento soubor nazveme VylepsovacZpravy.aj, určuje nám logiku vkládání aspektu do kódu.

Nejprve se pomocí klíčového slova pointcut určí místo, kam bude aspekt vložen a pak se před (before) a za (after) volání metody napisZpravu vloží další kód.


[remark]:<slide>(new)
### Výsledek voláni
V hlavní třídě aplikace vytvoříme nejprve instanci pisatele a potom proxy tohoto cílového objektu.

Ještě před vytvořením proxy přidáme advice vylepšovač pomocí metody addAdvice.

Dále metodou setTarget nastavíme cílový objekt (PisatelZpravy).

Pomocí metody getProxy vytvoříme proxy a nyní již můžeme volat metodu napisZpravu.
Výsledek vypadá takto:

```
Dobrý den
Ahoj Karle!
Nashledanou
```

Podrobnější příklad na samostudium [zde](http://www.springboottutorial.com/spring-boot-and-aop-with-spring-boot-starter-aop)