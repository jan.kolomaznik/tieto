package cz.mendelu.pjj.robot.swing;

import com.sun.xml.internal.stream.buffer.sax.DefaultWithLexicalHandler;
import cz.mendelu.pjj.robot.Robot;
import cz.mendelu.pjj.robot.World;
import cz.mendelu.pjj.robot.greenfoot.RobotWorld;
import greenfoot.Greenfoot;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MenuForm {

    static {
        try {
            // Set cross-platform Java L&F (also called "Metal")
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JFrame frame;

    private JPanel rootPanel;
    private JButton saveButton;
    private JButton loadButton;
    private JButton newButton;
    private JButton exitButton;

    private World world;
    private Robot robot;

    public MenuForm(World world, Robot robot) {
        this.world = world;
        this.robot = robot;
        this.frame = new JFrame("Setting frame");
        this.frame.setContentPane(rootPanel);
        this.frame.pack();
        this.frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                hideMenu(false);
            }
        });
        newButton.addActionListener(this::newAction);
        saveButton.addActionListener(this::savaAction);
        loadButton.addActionListener(this::loadAction);
        exitButton.addActionListener(this::exitAction);
    }

    public void openMenu() {
        Greenfoot.stop();
        SwingUtilities.invokeLater(() -> {
            frame.setVisible(true);
        });
    }

    public void hideMenu(boolean dispose) {
        Greenfoot.start();
        SwingUtilities.invokeLater(() -> {
            frame.setVisible(false);
            if (dispose) {
                frame.dispose();
            }
        });
    }

    public void newAction(ActionEvent e) {
        Greenfoot.setWorld(new RobotWorld());
        hideMenu(true);
    }

    public void exitAction(ActionEvent e) {
        System.exit(0);
    }

    public void savaAction(ActionEvent e) {
        // FIXME Upravit v samostatné práci
    }

    public void loadAction(ActionEvent e) {
        // FIXME Upravit v samostatné práci
    }

}
