package cz.mendelu.pjj.robot;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.*;

import static cz.mendelu.pjj.robot.Point.point;

/**
 * Created by Honza on 08.11.2016.
 */
public class World {

    private final int width;
    private final int height;
    private Map<Point, Object> map;

    public World(int width, int height) {
        this.width = width;
        this.height = height;
        this.map = new HashMap<>();
    }

    public Object getTreasureAt(int x, int y){
        return map.get(point(x, y));
    }

    public void removeTreasureAt(int x, int y){
        map.remove(point(x, y));
    }

    public Object addTreasureAt(Object object, int x, int y){
        map.put(point(x, y), object);
        return object;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
