package cz.mendelu.pjj.robot;

import static cz.mendelu.pjj.robot.Point.point;

/**
 * Created by Honza on 09.11.2016.
 */
public enum Direction {

    NORTH(0, -1),
    NORTH_EAST(1, -1),
    EAST(1, 0),
    SOUTH_EAST(1, 1),
    SOUTH(0, 1),
    SOUTH_WEST(-1, 1),
    WEST(-1, 0),
    NORTH_WEST(-1, -1);

    private static int COUNT = Direction.values().length;

    private final int dx, dy;

    Direction(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public Point moveFrom(Point p) {
        return p.diff(dx, dy);
    }

    public Direction onRight() {
        int index = (ordinal() + 1) % COUNT;
        return values()[index];
    }

    public Direction onLeft() {
        int index = (ordinal() - 1 + COUNT) % COUNT;
        return values()[index];
    }

}
