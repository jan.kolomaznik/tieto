package cz.mendelu.pjj.robot;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import static cz.mendelu.pjj.robot.Point.point;

/**
 * Created by Honza on 08.11.2016.
 */
public class Robot {

    private Point point;
    private Direction direction;

    private PropertyChangeSupport pcs;

    public Robot(int x, int y, Direction direction) {
        this.point = point(x, y);
        this.direction = direction;
        this.pcs = new PropertyChangeSupport(this);
    }

    public void forward() {
        Point oldPoint = point;
        point = direction.moveFrom(point);
        pcs.firePropertyChange("point", oldPoint, point);
    }

    public void turnLeft() {
       Direction oldDirection = direction;
       direction = direction.onLeft();
       pcs.firePropertyChange("direction", oldDirection, direction);
    }

    public void turnRight() {
        Direction oldDirection = direction;
        direction = direction.onRight();
        pcs.firePropertyChange("direction", oldDirection, direction);
    }

    public int[] getPosition() {
        return new int[] {point.getX(), point.getY()};
    }

    public Direction getDirection() {
        return direction;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }
}

