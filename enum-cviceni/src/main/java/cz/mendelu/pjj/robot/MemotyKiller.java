package cz.mendelu.pjj.robot;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MemotyKiller {

    public static void main(String[] args) {
        int i = 0;
        List<Point> points = new LinkedList<>();
        System.out.println(Runtime.getRuntime().maxMemory() / (1024 * 1024) + " MB");
        while (true) {
            try {
                points.add(Point.point(i, i++));
                if (i % 100_000 == 0) {
                    String free = " " + Runtime.getRuntime().freeMemory() / (1024 * 1024) + " MB";
                    System.out.println(i + free);
                }
            } catch (Error e) {
                System.err.println(i);
            }
        }
    }
}
