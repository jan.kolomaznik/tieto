package cz.mendelu.pjj.robot;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Point {

    public static final Point NULL = new Point(0, 0) {


        @Override
        public Point diff(int dx, int dy) {
            return this;
        }

        @Override
        public boolean equals(Object o) {
            return o == this;
        }

        @Override
        public int hashCode() {
            return 0;
        }

        @Override
        public String toString() {
            return "Null point";
        }
    };

    private static final Map<String, Point> instanceMap = new HashMap<>();

    private static long counter;

    public static Point point(int x, int y) {
        String key = x + ":" + y;
        Point result = null;
        if (!instanceMap.containsKey(key)) {
            synchronized (Point.class) {
                if (!instanceMap.containsKey(key)) {
                    result = new Point(x, y);
                    instanceMap.put(key, result);
                }
            }
        } else {
            result = instanceMap.get(key);
        }
        return result;
    }

    private final int x, y;

    private Point(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.format("%d: %s\n", counter++, this);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point diff(int dx, int dy) {
        return point(x + dx, y + dy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
