package cz.mendelu.pjj.robot.greenfoot;

import cz.mendelu.pjj.robot.Direction;
import cz.mendelu.pjj.robot.Robot;
import cz.mendelu.pjj.robot.World;
import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.MouseInfo;
import greenfoot.util.GraphicsUtilities;
import greenfoot.util.GreenfootUtil;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import static java.lang.String.format;

public class RobotWorld extends greenfoot.World {

    private static Logger log = Logger.getGlobal();

    private static final int WORLD_SIZE = 7;
    private static final int WORLD_CELL = 64;

    private final Robot robot;
    private final World world;

    public RobotWorld() {
        this(new World(WORLD_SIZE, WORLD_SIZE),
                new Robot(3, 0, Direction.SOUTH));

    }

    public RobotWorld(World world, Robot robot) {
        super(world.getWidth(), world.getHeight(), WORLD_CELL);
        this.robot = robot;
        this.world = world;

        robot.addPropertyChangeListener(evt -> System.out.println(evt));

        setBackground("images/world-background.png");
        addObject(new RobotActior(robot), 0, 0);
        addObject(new FilesActor(world, robot), 0, 0);
        update();
    }

    @Override
    public void act() {
        MouseInfo mouseInfo = Greenfoot.getMouseInfo();
        if (mouseInfo != null && mouseInfo.getClickCount() == 1 && mouseInfo.getActor() == null) {
            String name = Greenfoot.ask("Vlož jméno pokladu: ");
            Object treasure = world.addTreasureAt(name, mouseInfo.getX(), mouseInfo.getY());
            log.info(String.format("Add Treasure at [%d, %d] name: %s.", mouseInfo.getX(), mouseInfo.getY(), treasure));
        }

        Object treasure = world.getTreasureAt(robot.getPosition()[0], robot.getPosition()[1]);
        if (treasure != null) {
            log.info(format("Robot found treasure %s.", treasure.toString()));
            world.removeTreasureAt(robot.getPosition()[0], robot.getPosition()[1]);
            this.addObject(new CheckActor(), robot.getPosition()[0], robot.getPosition()[1]);
        }

        update();
    }

    private void update() {
        // Update treasurers
        List<TreasureActor> treasureActors = getObjects(TreasureActor.class);
        for(int x = 0; x < world.getWidth(); x++) {
            for(int y = 0; y < world.getHeight(); y++) {
                Object treasure = world.getTreasureAt(x, y);
                if (treasure != null) {
                    TreasureActor actor = remove(treasureActors, treasure);
                    if (actor == null) {
                        addObject(new TreasureActor(treasure), x, y);
                    }
                }
            }
        }
        removeObjects(treasureActors);

    }

    private static TreasureActor remove(List<TreasureActor> treasureActors, Object treasure) {
        Iterator<TreasureActor> iterator = treasureActors.iterator();
        while (iterator.hasNext()) {
            TreasureActor actor = iterator.next();
            if (actor.getTreasure().equals(treasure)) {
                iterator.remove();
                return actor;
            }
        }
        return null;
    }
}