package cz.mendelu.pjj.robot.greenfoot;

import cz.mendelu.pjj.robot.Robot;
import cz.mendelu.pjj.robot.World;
import cz.mendelu.pjj.robot.swing.MenuForm;
import greenfoot.Actor;
import greenfoot.Greenfoot;

import javax.swing.*;

public class FilesActor extends Actor {

    private final MenuForm menuForm;

    public FilesActor(World world, Robot robot) {
        setImage("images/files.png");
        menuForm = new MenuForm(world, robot);
    }

    @Override
    public void act() {
        if (Greenfoot.mouseClicked(this)) {
            menuForm.openMenu();
        }
    }
}
