package cz.ictpro.tieto;

import java.util.EnumSet;

public class EMain {

    public static void main(String[] args) {
        System.out.println("minus: " + EOperation.MINUS.getCh());

        EOperation x = EOperation.PLUS;
        if (x == EOperation.MINUS) {
            System.out.println("Je to minus");
        } else {
            System.out.println("Nevim co to je: " + x.name());
        }

        System.out.println("Result: " + x.calculate(1, 2));
        System.out.println("Result: " + Operation.MINUS.calculate(1, 2));

        EnumSet enumSet = EnumSet.of(EOperation.MINUS);
        enumSet.add(EOperation.PLUS);
        System.out.println(enumSet);
    }
}
