package cz.ictpro.tieto;

public class Main {

    public static void main(String[] args) {
        System.out.println("minus: " + Operation.MINUS.getCh());

        Operation x = Operation.PLUS;
        if (x == Operation.MINUS) {
            System.out.println("Je to minus");
        } else {
            System.out.println("Nevim co to je: " + x);
        }

        System.out.println("Result: " + x.calculate(1, 2));
        System.out.println("Result: " + Operation.MINUS.calculate(1, 2));
    }
}
