package cz.ictpro.tieto;

public enum EOperation {

    PLUS('+') {
        @Override
        public int calculate(int x, int y) {
            return x + y;
        }
    }, MINUS('-') {
        @Override
        public int calculate(int x, int y) {
            return x - y;
        }
    };

    private final char ch;

    EOperation(char ch) {
        this.ch = ch;
    }

    public char getCh() {
        return ch;
    }

    public abstract int calculate(int x, int y);

    public int invert(int x, int y) {
        return calculate(y, x);
    }
}
