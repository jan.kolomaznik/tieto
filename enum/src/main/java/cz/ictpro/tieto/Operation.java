package cz.ictpro.tieto;

public abstract class Operation {

    public static final Operation PLUS = new Operation('+') {

        @Override
        public int calculate(int x, int y) {
            return x + y;
        }
    };

    public static final Operation MINUS = new Operation('-') {
        @Override
        public int calculate(int x, int y) {
            return x - y;
        }

        @Override
        public int invert(int x, int y) {
            return y - x;
        }
    };

    private final char ch;

    private Operation(char ch) {
        this.ch = ch;
    }

    public char getCh() {
        return ch;
    }

    public abstract int calculate(int x, int y);

    public int invert(int x, int y) {
        return calculate(y, x);
    }
}
