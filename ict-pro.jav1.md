JAVA - ÚVOD DO PROGRAMOVÁNÍ 
===========================
 
Kurz účastníky uvede do konceptů objektově orientovaného programování a základů programovacího jazyku Java. 
Účastníci se naučí vytvářet základní aplikace založené na technologii Java pomocí různých objektově orientovaných technik.

Osnova kurzu 
------------ 
 
- **Úvod, seznámení s programovacím jazykem Java**
- **Úvod do objektově orientovaného programování**
  - Seznámení s pojmy objekt, dědičnost, třída, polymorfismus
  - Základní principy OOP
- **Seznámení s vývojovým prostředím (standardně Netbeans, po domluvě i jiné)**
- **Datové typy, operátory, řídící příkazy**
- **Pole a řetězce - jejich zpracování**
  - Vlastnosti řetězců, porovnávání řetězců, imutabilita
  - Metody třídy String
  - Standardní jazykové pole, omezení a jejich výhody, vytváření, cyklus for a iteratovatelné objekty
- **Návrh a tvorba tříd, metod, objektů**
  - Vytvoření vlastní třídy
  - Proměnné objektu, metody objektu
  - Vytváření objektů pomocí new
  - Úrovně viditelnosti
- **Vytváření balíků a implementace rozhraní**
  - Struktura balíků, vztah balíku a adresářové struktury při překladu a natahování tříd, význam IDE pro automatickou správu adresářů a balíků
  - Importování balíků, standardní viditelnost typů
  - Implementace rozhraní, automatické přetypování na rozhraní, explicitní přetypování, význam operátoru instanceof
- **Zpracování výjimek v programu**
  - Typy výjimek v Javě
  - Standardní výjimky, kontrolované a běhové výjimky
  - Ošetřování výjimek
  - Blok finally
- **Vstupní a výstupní operace**
  - Práce s konzolí, výpis na obrazovku, čtení od uživatele, objekt Console
  - Parametry programu, oživení aplikace řízené z příkazového řádku
- **Tvorba a oživení samostatných aplikací**