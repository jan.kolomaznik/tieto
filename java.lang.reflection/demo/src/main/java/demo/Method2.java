package demo;

import java.lang.reflect.*;

public class Method2 {

    public int add(int a, int b) {
        return a + b;
    }

    public static void main(String args[]) {
        try {
            Class cls = Class.forName("demo.Method2");
            Class partypes[] = {Integer.TYPE, Integer.TYPE};
            Method meth = cls.getMethod("add", partypes);
            Method2 methobj = (Method2) cls.newInstance();
            Object arglist[] = {new Integer(37), new Integer(47)};
            Object retobj = meth.invoke(methobj, arglist);
            Integer retval = (Integer)retobj;
            System.out.println(retval.intValue());
        } catch (Throwable e) {
            System.err.println(e);
        }
    }
}
