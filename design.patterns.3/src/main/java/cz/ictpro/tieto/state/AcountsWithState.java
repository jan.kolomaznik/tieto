package cz.ictpro.tieto.state;

public class AcountsWithState {

    interface State {
        void putMoney(long amount);
        void drawMoney(long amount);
        void takeLoan(long amount);
    }

    private class DebuteState implements State {

        @Override
        public void putMoney(long amount) {
            balance += amount;
        }

        @Override
        public void drawMoney(long amount) {
            if (balance >= amount) {
                balance -= amount;
            }
        }

        @Override
        public void takeLoan(long amount) {
            loan = amount;
            state = new LoanState();
        }
    }

    private class LoanState implements State {

        @Override
        public void putMoney(long amount) {
            loan -= amount;
            if (loan <= 0) {
                balance = -loan;
                loan = 0;
                state = new DebuteState();
            }
        }

        @Override
        public void drawMoney(long amount) {
            if (loan >= amount) {
                loan -= amount;
            }
        }

        @Override
        public void takeLoan(long amount) {
            throw new UnsupportedOperationException("Nejprve zplat pujci");
        }
    }

    private long balance;
    private long loan;

    private State state = new DebuteState();

    public void putMoney(long amount) {
        state.putMoney(amount);
    }

    public void drawMoney(long amount) {
        state.drawMoney(amount);
    }

    public void takeLoan(long amount) {
        state.takeLoan(amount);
    }
}
