package cz.mendelu.pjj.kolekce;
/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Student {

    private final String jmeno;
    private final String prijmeni;

    public Student(String jmeno, String prijmeni) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
    }
}
