package cz.mendelu.pjj.kolekce

/**
 * Created by xkoloma1 on 02.11.2016.
 */
class Ukol_4_Test extends GroovyTestCase {

    public void "test opakovaneho pridani"() {
        Predmet predmet = new Predmet("PJJ");

        assert predmet.zapisDoPredmetu(new Student("Tomaš", "Novak")) == true;
        assert predmet.zapisDoPredmetu(new Student("Tomaš", "Novak")) == false;
    }

    public void "test serazeni studentu v predmetu"() {
        Predmet predmet = new Predmet("PJJ");
        predmet.zapisDoPredmetu(new Student("Tomaš", "Novak"));
        predmet.zapisDoPredmetu(new Student("Jan", "Vopička"));
        predmet.zapisDoPredmetu(new Student("Jan", "Adamek"));
        predmet.zapisDoPredmetu(new Student("Tomaš", "Novak"));

        Iterator<Student> studentIterator = predmet.getStudents();

        assert studentIterator.next().equals(new Student("Jan", "Adamek"));
        assert studentIterator.next().equals(new Student("Tomaš", "Novak"));
        assert studentIterator.next().equals(new Student("Jan", "Vopička"));
        assert studentIterator.hasNext() == false;
    }

    public void "test radneho prihlaseni studenta do cviceni"() {
        Predmet predmet = new Predmet("PJJ");
        Student student = new Student("Tomaš", "Novak")
        Cviceni cviceni = predmet.zalozCviceni(10);

        predmet.zapisDoPredmetu(student);
        assert cviceni.prihlasit(student) == true;
    }

    public void "test prihlaseni studenta do cviceni kdyz neni zapsín do predmetu"() {
        Predmet predmet = new Predmet("PJJ");
        Student student = new Student("Tomaš", "Novak")
        Cviceni cviceni = predmet.zalozCviceni(10);

        assert cviceni.prihlasit(student) == false;
    }


}
