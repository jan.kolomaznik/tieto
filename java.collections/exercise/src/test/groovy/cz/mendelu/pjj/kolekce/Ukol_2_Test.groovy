package cz.mendelu.pjj.kolekce

/**
 * Created by xkoloma1 on 02.11.2016.
 */
class Ukol_2_Test extends GroovyTestCase {

    public void "test zalozeni cviceni v predmetu"() {
        Predmet predmet = new Predmet("PJJ");

        Cviceni cviceni = predmet.zalozCviceni(10);

        assert cviceni != null;
        assert cviceni.kapacita == 10;
    }

    public void "test ziskání seznamu cviceni na predmetu"() {
        Predmet predmet = new Predmet("PJJ");

        Cviceni prvni = predmet.zalozCviceni(10);
        Cviceni druhe = predmet.zalozCviceni(20);

        assert predmet.getCvicenis().size() == 2;
    }

    public void "test ziskani jednoho cviceni v predmetu"() {
        Predmet predmet = new Predmet("PJJ");

        Cviceni prvni = predmet.zalozCviceni(10);
        Cviceni druhe = predmet.zalozCviceni(20);

        assert predmet.getCviceni(0) == prvni;
        assert predmet.getCviceni(1) == druhe;
    }

    public void "test nelegalniho zalozeni cviceni"() {
        Predmet predmet = new Predmet("PJJ");
        Cviceni evilLession = new Cviceni(Integer.MAX_VALUE);

        shouldFail(RuntimeException) {
            predmet.getCvicenis().add(evilLession);
        }
    }
}
