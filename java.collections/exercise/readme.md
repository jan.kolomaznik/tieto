[remark]:<class>(center, middle)
# Kolekce a enumy

[remark]:<slide>(new)
## 1. Přípravná fáze

Stáhněte šablonu na dnešní projekt **[template.zip](template.zip)** pokračujte v něm.

[remark]:<slide>(wait)
1. Vytvořte třídu `Student`, která …

  - Bude obsahovat `String jmeno` a `String prijmeni`, tyto atributy každého studenta jednoznačně identifikují

2. Vytvořte třídu `Cviceni`, která …

  - Bude obsahovat atribut `int kapacita`

3. Vytvořte třídu `Predmet`, která …

  - Bude jednoznačně identifikována atributem `String kod`
  
[remark]:<slide>(new)
#### 1. Úkol
Vytvořte metody objektu `Cviceni`, které 

- Umožní přihlásit studenta do cvičení, pokud to umožní kapacita cvičení: `boolean prihlasit(Student student)`

- Vrátí kolekci zapsaných studentů: `Collection<Student> getStudents()`

[remark]:<slide>(new)
#### 2. Úkol
Vytvořte metody objektu `Predmet`, které:

- Umožní v rámci předmětu vytvořit cvičení (cvičení jsou v rámci předmětu jednoznačně určena svým indexem): `Cviceni zalozCviceni(int kapacita)`

- Vrátí kolekci cvičení `Collection<Cviceni> getCvicenis()`

- Vrátí Cvičení: `Cviceni getCviceni(int index)`
  
[remark]:<slide>(new) 
#### 3. Úkol
Vytvořte metody objektu `Predmet`, které

- Umožní zapsat studenta do předmětu: `boolean zapisDoPredmetu(student)`

- Vrátí abecedně setřesenou kolekci všech zapsaných studentů: `Collection<Student> getStudents()`

[remark]:<slide>(new) 
#### 4. Úkol
Vytvořte metody objektu Predmet, které

- Umožní přihlásit studenta do cvičení, pokud je zapsán v předmětu a v rámci předmětu se může student přihlásit pouze do jednoho cvičení.

- Vypíše všechny studenty, kteří nejsou zapsáni do žádného cvičení: `Collection<Student> getStudentyBezCviceni()`


