package demo;

import java.util.Date;
import java.util.concurrent.*;

public class Executori7 {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

        Runnable task = () -> System.out.println("Scheduling: " + new Date());

        int initialDelay = 1;
        int period = 3;
        executor.scheduleAtFixedRate(task, initialDelay, period, TimeUnit.SECONDS);
    }
}
