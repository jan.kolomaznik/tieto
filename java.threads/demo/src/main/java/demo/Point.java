package demo;

import java.util.Arrays;
import java.util.Random;

public class Point {

    private int x,y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int[] get() {
        synchronized (this) {
            System.out.println("get: " + Thread.currentThread().getName());
            return new int[]{x, y};
        }
    }

    public  void set(int x, int y) {
        synchronized (this) {
            System.out.println("set: " + Thread.currentThread().getName());
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args) {
        Random random = new Random();
        Point p = new Point(0,0);
        Runnable r = () -> {
            while (true) {
                int x = random.nextInt();
                int y = random.nextInt();

                synchronized (p) {
                    System.out.println("In: " + x + "," + y);
                    p.set(x, y);
                    int[] xy = p.get();
                    System.out.println(Arrays.toString(xy));
                }
            }
        };

        new Thread(r, "Vlakni 1").start();
        new Thread(r, "Vlakni 2").start();
    }
}
