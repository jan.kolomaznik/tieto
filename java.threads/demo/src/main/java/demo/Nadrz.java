package demo;

public class Nadrz {

    public static final Object WATER_MONITOR = new Object();

    private long obsah = 1_000_000;

    public synchronized long getObsah() {
            return obsah;
    }

    public synchronized void inc() {
            this.obsah++;
    }

    public synchronized void dec() {
            this.obsah--;
    }
}
