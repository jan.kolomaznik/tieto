package demo;

public class Vlakno extends Thread {

    private int value = -1;

    public Vlakno(String jmeno) {
        super(jmeno);
    }

    public void run() {
        for (int i = 1; i <= 3; i++) {
            System.out.println(i + ". " + getName());
            value = i;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Jsem vzhuru - " + getName());
            }
        }
    }

    public static void main(String[] args) {
        Vlakno vl = new Vlakno("Pepa");
        vl.setDaemon(true);
        Thread startStop = new Thread(() -> {
            System.out.println("Start");
            vl.start();
            try {
                vl.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Stop");
        });
        startStop.start();
    }
}