package topic.lombok;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Data
@RequiredArgsConstructor(staticName = "employee")
public class Employee {

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        Employee person = Employee.employee("Tomas", "Suoer");
    }
}
