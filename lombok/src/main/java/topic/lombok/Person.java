package topic.lombok;

import lombok.Data;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

//@Data
@Slozena(name = "Ahoj")
public class Person {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;


    public static void main(String[] args) {
        System.out.println(new Person().toString());
        Arrays.stream(Person.class.getDeclaredAnnotations()).forEach(System.out::println);
    }
}
