package topic.lombok;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.Arrays;

@AllArgsConstructor
public class PersonLombok_1 {

    @NonNull
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        //new PersonLombok_1(null, "Kumar", LocalDate.now());

        Arrays.stream(PersonLombok_1.class.getDeclaredMethods()).forEach(System.out::println);
    }
}
