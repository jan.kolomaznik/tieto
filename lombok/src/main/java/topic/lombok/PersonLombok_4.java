package topic.lombok;

import lombok.Data;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.LocalDate;
import java.util.Arrays;

@Value
public class PersonLombok_4 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_4 person = new PersonLombok_4("Pepa", "Zdepa", null);
        System.out.println(person);


        Arrays.stream(PersonLombok_4.class.getDeclaredMethods()).forEach(System.out::println);
    }
}

