package topic.lombok;

import jdk.nashorn.internal.objects.annotations.Getter;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR})
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Getter
public @interface Slozena {

    String name() default "";
}
