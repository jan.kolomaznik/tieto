package topic.lombok;

import lombok.extern.java.Log;

@Log
public class LogEmployee {

    public void work(){
        log.info("Log method");
    }

    public static void main(String[] args) {
        LogEmployee employee = new LogEmployee();
        employee.work();
    }
}
