package topic.lombok;

import lombok.*;

import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Arrays;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class PersonLombok_2 {

    @NonNull
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        new PersonLombok_2();
        //PersonLombok_2 person = new PersonLombok_2("Pepa", "Zdepa", LocalDate.now());
        //person.setFirstName("Tomas");
        //System.out.println("firstName: " + person.getFirstName());
        //System.out.println("lastName: " + person.getLastName());

        Arrays.stream(PersonLombok_2.class.getDeclaredMethods()).forEach(System.out::println);
    }
}

