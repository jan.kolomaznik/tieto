package topic.lombok;

import lombok.NonNull;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.Wither;

import java.time.LocalDate;

@Value @Wither
public class ImmutableEmployee {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        ImmutableEmployee person = new ImmutableEmployee("Pepa", "Zdepa", null);
        System.out.println(person
                .withDateOfBirth(LocalDate.now())
                .withFirstName("Honza"));
        System.out.println(person);

    }
}
