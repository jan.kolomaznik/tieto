package demo.io;

import java.io.BufferedWriter;
import java.io.Console;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class DemoWriter {

    public static void main(String[] args) {
        try (BufferedWriter bw = new BufferedWriter(new MorseWriter(new FileWriter("message.txt"))) ) {
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                bw.write(scanner.nextLine());
                bw.newLine();
                bw.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
