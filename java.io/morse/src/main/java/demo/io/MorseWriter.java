package demo.io;

import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;

public class MorseWriter extends Writer {

    private static enum Last {
        LETTER, WORLD, SENTENS;
    }

    private Writer decoratedWriter;
    private MorseCode morseCode;

    public MorseWriter(Writer writer) {
        this.decoratedWriter = writer;
        this.morseCode = new MorseCode();
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        Last last = null;
        int stop = Math.min(off + len, cbuf.length);
        for (int i = off; i < stop; i++) {
            char ch = Character.toUpperCase(cbuf[i]);
            if (Character.isAlphabetic(ch)) {
                String code = morseCode.encode(ch);
                if (last == Last.LETTER) {
                    decoratedWriter.append(morseCode.getLetterSeperator());
                }
                decoratedWriter.append(code);
                last = Last.LETTER;
            } else if (ch == ' ') {
                if (last == Last.LETTER) {
                    decoratedWriter.append(morseCode.getWorldSeperator());
                }
                last = Last.WORLD;
            } else if (ch == '.') {
                if (last == Last.LETTER) {
                    decoratedWriter.append(morseCode.getSentensSeperator());
                }
                last = Last.SENTENS;
            }
        }
    }

    @Override
    public void flush() throws IOException {
        decoratedWriter.flush();
    }

    @Override
    public void close() throws IOException {
        decoratedWriter.close();
    }
}
