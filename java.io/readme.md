[remark]:<class>(center, middle)
# I/O operace

[remark]:<slide>(new)
## Úvod
* Pro práci se soubory na disku, pro síťovou komunikaci, pro komunikaci mezi vlákny apod. potřebujeme mechanismus přenosu/přesunu dat.

* Základní způsob realizace vstupně-výstupních operací v Javě – pomocí streamů („proudů“).

* Stream si lze představit jako trubku, jejíž konec máme k dispozici a můžeme „čerpat“ data z něho (tedy číst) nebo naopak do něho (tj. zapisovat).

* Stream vytvoříme jako objekt, tedy instanci určité třídy.

* Základní třídy pro práci se streamy obsahuje balík `java.io`.

* Známe dvě hlavní kategorie streamů: binární a textové. Textové jsou specializované na vstup/výstup textu, binární na obecný proud bajtů.

* Binární vstupní streamy jsou odvozeny od abstraktní třídy `InputStream`, výstupní od třídy `OutputStream`.

* Textové vstupní streamy jsou odvozeny od abstraktní třídy `Reader`, výstupní od třídy `Writer`.

[remark]:<slide>(new)
### Životní cyklus streamu – 4 etapy:
* **VYTVOŘENÍ** (zavolání konstruktoru) – může se vytvářet přímo tam, kde se používá, anebo ho může vytvořit nějaký jiný objekt a předat (např. jako parametr metody nebo naopak jako její návratovou hodnotu).

* **OTEVŘENÍ** – často už při vytváření, v každém případě však dříve, než se začne používat. 
  Otevření znamená, že se alokují potřebné systémové prostředky a stream se připraví pro práci.

* **POUŽITÍ** streamu. Se streamem se provádějí požadované operace, tzn. volají se jeho metody.

* **UZAVŘENÍ** – velmi důležité, často se na to zapomíná. 
  Pokud neuzavřeme, vyčerpávají se systémové prostředky (file deskriptory apod.) a za druhé může mnoho dat zůstat nezapsaných (u výstupních streamů). 
  Streamy proto zavíráme ihned, když s nimi přestáváme pracovat.

[remark]:<slide>(new)
### Vybrané druhy streamů
* K zápisu na výstup slouží metoda instance write(), ke čtení ze vstupu metoda instance read():

![](media/basicStreams.gif)

* Přestože se se všemi streamy pracuje prakticky stejně, určité rozdíly jsou v jejich ůčelu a tím i způsobu přípravy. 

* Používají se například tyto druhy streamů:

[remark]:<slide>(new)
#### Souborové streamy
* Slouží ke čtení a zápisu souborů. 

* Stream se otevře, v cyklu se z něho čte po bajtech (pozor – i když se čtou bajty, hodnota je typu int). 
  
* Pokud je přečtena hodnota −1, bylo dosaženo konce souboru). Všechny operace jsou uzavřeny do bloku try k zachycení výjimek. 

* Streamy jsou instancemi tříd:
  - `FileInputStream` pro čtení proudu bajtů ze souboru,
  - `FileOutputStream` pro zápis proudu bajtů do souboru,
  - `FileReader` pro čtení textu z textového souboru,
  - `FileWriter` pro zápis textu do textového souboru.

```java
try(FileInputStream fis = new FileInputStream("soubor.dat")) {
  // stream se hned otevře
  int i = 0;
  while ((i = fis.read()) >= 0) {      // čte se, dokud není konec souboru
      ...
  }
} catch (IOException e) {
    ...         // zpracování výjimky
}
```

[remark]:<slide>(new)
#### Řetězcové streamy
* Umožňují snadno číst z řetězce a zapisovat do něj streamovým způsobem. 

* Stream pracuje nad objektem typu `StringBuffer`, ke kterému můžeme získat přímý přístup – lze ale také ze streamu odvodit instanci třídy `String`. 

* Streamy jsou instancemi tříd `StringReader` a `StringWriter`, používají se opět metody `read()` a `write()`. 

```java
StringWriter sw = new StringWriter();
sw.write("abcd");
System.out.println(sw);
```

[remark]:<slide>(new)
#### Filtrové streamy 
* Skupina streamů, které slouží v podstatě k libovolnému zpracování procházejících dat. 

* Typickým představitelem jsou bufferované streamy, které implementují buffer (vyrovnávací paměť). 

* Například zápis do souboru, který by probíhal po samostatných bajtech, se provádí najednou až v okamžiku, kdy je jich připraveno víc. 

* Používáme třídy `BufferedInputStream`, `BufferedOutputStream`, `BufferedReader` a `BufferedWriter`.

```java
try (BufferedReader br = new BufferedReader(new FileReader("soubor.txt"))) {
    String s = "";
    while ((s = br.readLine()) != null) {
        ...
    }
} catch (IOException e) {
    ...
}
```

* Při použití bufferovaných výstupních streamů není zaručeno, kdy se data z bufferu přesunou do navazujícího streamu. 

* K zajištění zápisu dat z bufferu proto v případě potřeby voláme metodu `flush()`.

[remark]:<slide>(new)
#### Roury (pipe)
* `PipedInputStream`/`PipedOutputStream` a `PipedReader`/`PipedWriter` streamy.

* Tyto dva páry streamů představují tzv. *rouru (pipe)*, což jsou vlastně vzájemně propojené streamy. 

* Vezmeme jeden vstupní a jeden výstupní stream, propojíme je a s každým koncem pracujeme úplně stejně, jako by to byl normální vstupní, resp. výstupní stream. 

* Nejčastějším použitím je komunikace mezi vlákny, kdy se v jednom vlákně vytvářejí nějaká data a současně se ve druhém tato data zpracovávají:

![](media/pipedStreams.gif)

* Tyto streamy se vytvářejí tak, že vytvoříme jeden z nich a druhému ho předáme jako argument v konstruktoru. 
  - Jinou cestou je vytvořit je nezávisle a pak na některém z nich zavolat metodu `connect()`
  
[remark]:<slide>(new)
#### Roury: první možnost
```java
PipedInputStream is = new PipedInputStream();
PipedOutputStream os = new PipedOutputStream(is);
```

[remark]:<slide>(wait)
#### Roury: druhá možnost
```java
PipedOutputStream os = new PipedOutputStream();
PipedInputStream is = new PipedInputStream(os);
```
  
[remark]:<slide>(wait)
#### Roury: třetí možnost
```java
PipedReader pr = new PipedReader();
PipedWriter pw = new PipedWriter();
pr.connect(pw);
```

[remark]:<slide>(new)
## Serializace a deserializace dat
* Serializace je konverze obecných dat (nějakým způsobem uložených) na proud bajtů tak, aby je šlo následně snadno zrekonstruovat. 
  Naopak deserializace je právě rekonstrukce proudu bajtů na data použitelná v programu. 

* Serializovat je nutno i hodnoty primitivních datových typů, např. `int` nebo `double`, k čemuž jsou připraveny třídy `DataInputStream` a `DataOutputStream`. 
  - Poskytují metody instance jako např. `void writeDouble(double v)` a `double readDouble()`.

![](media/streams.gif)

[remark]:<slide>(new)
## Serializace pomoci `DataOutputStream`
```java
int i = 165;
float f = 0.35;

try (DataOutputStream os = new DataOutputStream(
                               new FileOutputStream("soubor.dat")))
{
    os.writeInt(i);    // bezpečné uložení hodnoty typu int
    os.writeFloat(f);  // bezpečné uložení hodnoty typu float
    os.close();
} catch (IOException e) {
    ...
}
```

[remark]:<slide>(new)
### Serializaci a deserializaci celých objektů
* Pro serializaci a deserializaci celých objektů máme třídy `ObjectInputStream` a `ObjectOutputStream`, které lze však použít i pro primitivní datové typy.

* Nelze ukládat všechny objekty. Nutnou podmínkou je, aby implementovaly rozhraní `Serializable` (pokud se pokusíme serializovat nevyhovující objekt, dočkáme se výjimky `NotSerializableException`). 

* Protože se instance serializuje i se všemi odkazovanými objekty, musí být i tyto serializovatelné, anebo musí být jejich referenční proměnné označené modifikátorem `transient` (tedy že nebudou uloženy).

* Na rozdíl od primitivních typů, u objektů lze při deserializaci zjistit jejich typ (a nejen to, k úspěšné deserializaci musí být k dispozici příslušná třída – jinak operace skončí výjimkou `ClassNotFoundException`. 
 
* Metoda `readObject()` sice vrací referenci na typ Object, ale třídu si můžeme zjistit voláním `getClass()` na vrácené instanci nebo operátorem `instanceof`, a následně přetypovat podle potřeby.

#### Notes
**Komplexní příklad Serializaci a deserializaci celých objektů**

```java
package geometrie;

import java.io.*;

public class Obdelnik implements Serializable {

    private double vyska, sirka;
    private String jmeno;

    public Obdelnik(double vyska, double sirka, String jmeno) {
        this.vyska = vyska;
        this.sirka = sirka;
        this.jmeno = jmeno;
    }

    public Obdelnik(String vyska, String sirka, String jmeno) throws NumberFormatException {
        this(Double.valueOf(vyska), Double.valueOf(sirka), jmeno);
    }

    @Override
    public String toString() {
        return "Obdelnik{" + "vyska=" + vyska + ", sirka=" + sirka + ", jmeno=" + jmeno + '}';
    }

    public static void main(String[] args) {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            Obdelnik obd = new Obdelnik(5, 8, "Oskar");
            fos = new FileOutputStream("c:\\temp\\obdelnik.tmp");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(obd);
            oos.writeChar('R');
            oos.close();
            fos.close();
            
            FileInputStream fis = new FileInputStream("c:\\temp\\obdelnik.tmp");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object obj = ois.readObject();
            if (obj instanceof Obdelnik) {
                Obdelnik obd2 = (Obdelnik) obj;
                System.out.println(obd2.toString());
            }
            char pismeno = ois.readChar();
            System.out.println(pismeno);
            ois.close();
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        } catch (FileNotFoundException ex) {
            System.out.println(ex.toString());
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
    }
}
```