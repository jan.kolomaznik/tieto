[remark]:<class>(center, middle)
# Generické typy

[remark]:<slide>(new)
## Generické typy
- Používají se k bližší specifikaci typu objektu.
- Nejčastější použití je právě v kontejnerech.
- Pracujeme-li v programu s nějakým seznamem, víme jenom, že je to seznam objektů a nic víc. 
  * Samozřejmě si můžeme bokem pamatovat, že tam ukládáme jenom řetězce, ale to nic nemění na tom, že runtime systému je srdečně jedno, jaké objekty do seznamu vkládáme. 
  * Stejně tak, chceme-li z tohoto seznamu číst, získáváme zase jenom obecné objekty, které musíme explicitně přetypovat na řetězec (tj. třídu `String`). 
  * A opět – ačkoliv jako programátoři víme, co v seznamu má být, obecně si nemůžeme být jisti, zda to tam skutečně je. 
  * Což vede buď k tomu, že se budeme ptát, zda získávaný objekt je skutečně řetězcem, a až poté jej přetypujeme, nebo budeme „riskovat“ runtime výjimku `ClassCastException`. 
  * Jak vidíme, nevýhody jsou, a to docela významné.

[remark]:<slide>(new)
## Generické typy
- Použití generických typů zaručuje typovou bezpečnost kolekce, to znamená, že pokud se pokusíme přidat do těchto kolekcí něco jiného, než jsme nadefinovali, je ohlášena chyba již při kompilaci. 
- Bez použití generických typů by se při snaze přetypovat prvek seznamu na nekompatibilní objektový typ projevila chyba až při běhu programu).
- Pro vytvoření seznamu s generickým typem použijeme následující příkaz:

```java
List <String> seznam = new ArrayList<String>(); 
```

```java
List <String> seznam = new ArrayList<>(); 
```

- Takto vytvořený seznam pracuje jen s objekty typu `String`. 
- Při získávání prvků ze seznamu je tedy nemusíme přetypovat.

```java
String name = seznma.get(1); 
```

[remark]:<slide>(new)
### Generické typy a procházení kolekcí
- Při procházení kolekcí, můžeme použít nový „foreach“ cyklus:

```java
for (String s : seznam) { 
 ... 
} 
```

- Obecná metoda na výpis prvků seznamu

- Prvky mohou být libovolného typu:

```java
public static void printList(List<?> list) {
   for (Object elem: list) {
      System.out.print(elem + " ");
   }
}

List<Integer> li = Arrays.asList(1, 2, 3);
List<String>  ls = Arrays.asList("one", "two", "three");
printList(li);
printList(ls);
```

[remark]:<slide>(new)
### Generické typy a suma
- Nyní si představme, že chceme metodu, která udělá z nějakého seznamu čísel jeho sumu. 

- Uvažujme tedy následující (a pomiňme možné přetečení nebo podtečení rozsahu double):

```java
public static double suma(List<Number> cisla) {
   double result = 0;
   for (Number e : cisla) {
      result += e.doubleValue();
   }
   return result;
}
```

[remark]:<slide>(new)
### Generické typy a přetypování
- Chceme-li však použit seznam `List<Integer>` jako parametr metody suma, nelze jej použít!!!

Které z následující přiřazení jsou zprávné?
```java
List<Object> list = null;
List<Object> lo = new ArrayList<String>();
List<String> ls = lo;
Collection<String> cs = ls;
```

##### Notes
Pouze první a druhá

[remark]:<slide>(wait)
- Třída Integer je sice podtřídou třídy Number, avšak třída (kolekce) List<Integer> není podtřídou třídy List<Number>. 
Správná definice hlavičky metody suma je:

```java
public static double suma(List<? extends Number> cisla) { 
 ... 
} 
```

- Pak je možné při volání metody suma použít jako parametr seznam jakýchkoliv objektů, které jsou potomky třídy Number. 
- Pokud by třída Number nebyla abstraktní, bylo by možné použít i její instance.

[remark]:<slide>(new)
![](media/compatibility.png)

[remark]:<slide>(new)
## Generické třídy
- Generické třídy umožňují ponechal volbu typu objektu (tedy třídy), se kterou bude třída pracovat, až na okamžik vytvoření instance. 
- Pak se ale lze již spolehnout na kompilátor, který zabrání použití nekompatibilních typů.

[remark]:<slide>(new)
#### Příklad, kdy NENÍ použit generický typ:

```java
class Trida {

   private Object prvekX;
   private Object prvekY;

   public Trida(Object valX, Object valY) {
      prvekX = valX;
      prvekY = valY;
   }

   public void setElements(Object valX, Object valY) {
      prvekX = valX;
      prvekY = valY;
   }

   public Object getElementX() {return prvekX;}
   
   public Object getElementY() {return prvekY;}
   
   public static void main(String[] args) {
      Trida t = new Trida("A", "B");
      ...
      Integer i = (Integer)t.getElementX();
      // Předchozí příkaz půjde zkompilovat,
      // ale za běhu způsobí výjimku ClassCastException.
      }   
}
```

[remark]:<slide>(new)
#### Příklad, kdy JE použit generický typ:

```java
class GenerickaTrida<T> {

   private T prvekX;
   private T prvekY;

   public GenerickaTrida(T valX, T valY) {
      prvekX = valX;
      prvekY = valY;
    }

    public void setElements(T valX, T valY) {
        prvekX = valX;
        prvekY = valY;
    }

   public T getElementX() {return prvekX;}
   
   public T getElementY() {return prvekY;}
    
   public static void main(String[] args) {
      GenerickaTrida<String> t = new GenerickaTrida<String>("A", "B");
      Integer i = (Integer)t.getElementX();
      // Předchozí příkaz nepůjde zkompilovat
      // kvůli chybě "incorvetible types".
   }
}
```

[remark]:<slide>(new)
## Generické metody

Generické nemusejí být celé třídy, ale pouze i jen metody.

```java
public static <T> Set<T> singleton(T value) {
    final Set<T> result = new HashSet<T>(1);
    result.add(value);
            return result;
}
```

[remark]:<slide>(wait)
Při vyvolání metody je pak možné uvés její generický typ.

```java
Set<String> set = Collections.<String> singleton("A");
```

Zadání argumentů musí být někdy specifikování, pokud kompilátor nemůže vyvodit všechny z kontextu (závisí také na verzi kompilátoru).

[remark]:<slide>(new)
#### Příklad 1:

Vytvořte metodu `fromArrayToCollection`, která vloží prvky z pole do kolekce.

```java
static void fromArrayToCollection(Object[] a, Collection<?> c) {
   for (Object o : a) {
      c.add(o); // chyba při překladu: incompatible types
   }
}
```

[remark]:<slide>(wait)

```java
static <T> void fromArrayToCollection(T[] a, Collection<T> c) {
   for (T o : a) {
      c.add(o); // kompilátor OK
   }
}
```

[remark]:<slide>(new)
#### Příklad 2:

Vytvořte metodu `zjistiTridu`, která zjistí/vypíše třídu generického parametru.

[remark]:<slide>(wait)
```java
public static <U> void zjistiTridu(U u){
   System.out.println("U: " + u.getClass().getName());
}
    
public static void main(String[] args) {
   zjistiTridu(5); // vypíše "U: java.lang.Integer"
   zjistiTridu(5L); // vypíše "U: java.lang.Long"
   zjistiTridu(5.0); // vypíše "U: java.lang.Double"
   zjistiTridu(5F); // vypíše "U: java.lang.Float"
   zjistiTridu(5 + ""); // vypíše "U: java.lang.String"
}
```

[remark]:<slide>(new)
#### Příklad 3:

Vytvořte generickou metodu `max`, která vrátí větší ze dvou prvku. 
Oba prvky umcí implementovat rozhraní `Comparable`, aby he bylo možné porovnat

[remark]:<slide>(wait)
```java
static <T extends Comparable> T max(T t1, T t2) {
   if (t1.compareTo(t2) > 0) {
      return t1;
   } else {
      return t2;
   }
}

public static void main(String[] args) {
   System.out.println(max("A", "B"));
   System.out.println(max(9, 4));
}
```

[remark]:<slide>(new)
#### Příklad 4:

Navrhněte metodu `copy`, která zkopíruje pevky z jednoho pole do druhého, přičemž generecké typy jsou ve vztahu dědičnosti.

[remark]:<slide>(wait)
```java
public <T, S extends T> void copy(List<T> dest, List<S> source) {
   ...
}
```

[remark]:<slide>(new)
## Generické konstruktor

```java
public class Foo {
    public <S extends DataInput & Closeable> Foo(S data) throws IOException {
        try (S input = data) {
            …
        }
    }
    …
}
```

**&**: Pomocí operátoru je možné nastavit omezení na parametru typu: 
- jiný typový parametr
- nebo nejvýše jedna třída 
- a nula nebo více rozhraní.

Extrémně vzácné použití.

[remark]:<slide>(wait)
Generování generických konstruktorů může explicitně specifikovat seznam argumentů typu konstruktoru (což je ještě vzácnější):

```java
new<DataOutputStream> Foo(…);
```

[remark]:<slide>(new)
## Generické výjimky

Typy odvozené z `Throwable` nemusí být generické, ale mohou mít generické metody.

Mechanizmus výjimek `throw` a `catch` nemohou pracovat s generickými typy.

Výjimky v klauzuli `throwa` naopak generické být mohou.

```java
<X extends Throwable> void check (Supplier<? extends X> e) throws X {
    if (…) {
        throw e.get();
    }
}
```

[remark]:<slide>(new)
# Souhrn

Podpora generik je bohatá pro správné řešení různých situací, ale přichází s určitou cenou: vývojáři musí pochopit pozadí

Generiky jsou obtížně zvládnutelné, ale logické
- Mnemonický: PECS = **P**roducer **E**xtends, **C**onsumer **S**uper

Generika stojí za zvládnutí a použití
- Snížená potřeba typu přetypování
- Zajistěte, aby byla zaručena bezpečnost, pokud se nepoužívá žádné přetypování typu
  - Bohužel existují případy, kdy se nelze přetypování vyhnout

Použijte casting velmi pečlivě a jen tehdy, když dokážete, že je to bezpečné - za to nesete plnou zodpovědnost.

[remark]:<slide>(new)
## Generika pod pokličkou

Generace byla představena v Javě 5

Těžká volba architektů JVM a jazyků, existovaly dvě výjimky:
- Změnit vše pro podporu generik bez kompromisů?
- Udržujte binární kompatibilitu, takže existující binární soubory mohou běžet hladce spolu s kódem pomocí generik?

[remark]:<slide>(wait)
*Binární kompatibilita vyhrál*

Generice jsou implementovány na úrovni kompilátoru, typové parametry jsou vymazány pro JVM (typ vymazání)

JVM nevidí ani nezajímá parametry typu
- Existují některé důsledky: generika nejsou dokonalá (a mají nějaké neočekávané omezení)

[remark]:<slide>(new)
## Reifiable a raw types

[remark]:<slide>(wait)
#### Reifiable typ

Typ, jehož typová informace je plně dostupná v době běhu
- Jedná se o primitivní a nesobecné typy, surové typy a invokace neohraničených zástupných znaků

[remark]:<slide>(wait)
#### Raw typ 

Název obecného typu bez argumentů typu
- Tím JVM vidí typy v době běhu
- Raw druhy se mohou objevit ve zdroji, ale jsou silně odrazováni (a podkopávají celý bod generik)
- Použití prvotřídních typů způsobuje, že kompilátor vydává varování (nekontrolované nebo nebezpečné obsazení atd.).

[remark]:<slide>(new)
## Implied limitations

Primární typy nejsou podporovány jako argumenty typu

[remark]:<slide>(wait)
Není možné vytvořit instance parametrů typu ani jejich pole

[remark]:<slide>(wait)
Nemožno deklarovat statická pole, jejichž typ je parametrem typu

[remark]:<slide>(wait)
Nelze vytvořit pole nepřehledných typů

[remark]:<slide>(wait)
Cast casting a instanceof nefunguje s generickými typy

[remark]:<slide>(wait)
Omezení výjimek

[remark]:<slide>(wait)
Metodické podpisy jsou založeny na syrových typech → žádné přetížení s obecnými parametry

[remark]:<slide>(wait)
Rekurze parametrů typu není možná

[remark]:<slide>(new)
## Raw × generic typy

[remark]:<slide>(wait)
Nepoužívejte surové typy (je-li to možné)
- Surové druhy popírají účel a výhody generik - generické typy mohou (a měly) být používány téměř vždy

[remark]:<slide>(wait)
Místo surového typu `T` použijte `T <Object>` nebo `T <?>`, Pokud potřebujete "obecnou" variantu
- Poznámka `<?> A <? rozšiřuje objekt>` je (téměř) stejný (jen gramatika jazyka přijímá někdy `<?>` pouze)

[remark]:<slide>(wait)
Vyhýbání se surovým typům znamená vyloučení polí
- Například pole může být požadována na nízké úrovni

[remark]:<slide>(new)
## Unchecked warnings

Vezměte varování vážně a nesnažte se je překonat, aniž byste pochopili důvod.

Pokud je to možné, zdokonalte svůj kód, abyste odstranili veškeré varování "unchecked" nebo "unsafe"
- To pomáhá zajistit a prokázat bezpečnost typu

Pokud **žádná** změna kódu nemůže pomoci ...
1. Upravte kód tak, že útočná část je izolovaná, zapouzdřená a co nejmenší
2. Aplikujte `@SuppressWarnings("unchecked")` na porušující kód - často se může vztahovat pouze na jednu proměnnou!
3. Přidat komentář vysvětlující, proč může být varování potlačeno bezpečně

[remark]:<slide>(new)
[remark]:<class>(center, middle)
## Next: snippets examples