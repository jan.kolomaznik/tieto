package cz.ictpro.lectures.java.generics;
import java.util.List;

/**
 * This demonstration shows <i>capturing capture</i> technique.
 */
public final class D03_CapturingCapture {

    /**
     * Swap two elements in a list.
     *
     * @param list
     *            the list whose elements shall be swapped; it must not be
     *            {@code null}
     * @param index1
     *            the index of the first element; it must be within the valid
     *            index range of the list
     * @param index2
     *            the index of the second element; it must be within the valid
     *            index range of the list
     *
     * @throws IndexOutOfBoundsException
     *             if some index is not within the range
     */
    public static void swap(List<?> list, int index1, int index2) {
        swapElements(list, index1, index2);
    }

    // Like the public version, but with additional type parameters
    private static <T> void swapElements(List<T> list, int index1, int index2) {
        list.set(index1, list.set(index2, list.get(index1)));
    }

    private D03_CapturingCapture() {
        throw new AssertionError();
    }
}
