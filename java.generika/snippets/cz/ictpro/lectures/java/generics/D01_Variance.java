package cz.ictpro.lectures.java.generics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Generics variance test.
 */
public final class D01_Variance {

    /**
     * Dummy method to demonstrate the generic variance syntax and problems.
     *
     * @param args
     *            the application arguments
     */
    @SuppressWarnings("unused")
    public static void main(String... args) {
        { // Classic array problem
            final String[] strings = { "A", "B" };
            final Object[] objects = strings;
            objects[0] = Integer.valueOf(1); // Runtime exception
            final String stringFromStrings = strings[0];
            System.out.println(stringFromStrings);
            final String stringFromObjects = (String) objects[0];
            System.out.println(stringFromObjects);

            List<Object> list = null;
            List<Object> lo = new ArrayList<String>();
            List<String> ls = lo;
            Collection<String> cs = ls;
        }

        { // With generics
            final List<String> strings = new ArrayList<>();
            // Following line is not acceptable
//            final List<Object> objects = strings;
//            objects.add(Integer.valueOf(1));
        }

        { // Generics assignments
            final List<String> ls = new ArrayList<>();
            final Collection<String> cs = ls;
            // Following lines are not acceptable
//            final List<Object> lo1 = new ArrayList<String>();
//            final List<Object> lo2 = ls;
        }

        { // Covariant wildcard
            final List<Integer> li = new ArrayList<>();
            final List<? extends Number> ln = li;
            // Following line is not acceptable
//            ln.add(Integer.valueOf(1));
            final Number n = ln.get(0);
        }

        { // Contravariant wildcard
            final List<Number> ln = new ArrayList<>();
            final List<? super Number> lx = ln;
            // Following line is not acceptable
            lx.add(Integer.valueOf(1));
            final Object o = lx.get(0);
        }
    }
}
