package cz.ictpro.lectures.java.generics;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * This demonstration shows how a generic exception can be used.
 *
 * @author Petr Dolezal
 */
public final class D02_GenericExceptions {

    /** The list of the items produced by this demo. */
    private static final List<Integer> ELEMENTS = Arrays.asList(5, 4, 3, 2, 1, 0, -1);

    /**
     * The main method of the demo application.
     *
     * @param args
     *            the command line arguments; it must not be {@code null}
     */
    public static void main(String... args) {
        process(new ReliableSource(ELEMENTS.iterator()));

        try {
            process(new UnreliableSource(new ReliableSource(ELEMENTS.iterator())));
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    // This method is coded once, although it may throw different kinds of exceptions -
    // so different throws clause needn't be a reason for copy & paste of a generic code
    private static <E extends Exception, T extends Source<E>> void process(T source) throws E {
        System.out.println("Processing " + source);

        try {
            while (true) {
                final Integer value = source.next();
                if (value == null) {
                    break;
                }

                System.out.println(value);
            }
        } finally {
            System.out.println();
            source.close();
        }
    }

    /**
     * This interface provides a unified access to an iterative data source.
     *
     * @param <E>
     *            the type of the exception that the data source can thrown upon
     *            an error
     */
    private interface Source<E extends Exception> extends AutoCloseable {

        /**
         * Get the next element if available.
         *
         * @return the next element, or {@code null} if no more elements are
         *         available
         *
         * @throws E
         *             if the next element could not be retrieved due to the
         *             underlying source error
         */
        Integer next() throws E;

        /**
         * @see java.lang.AutoCloseable#close()
         */
        void close() throws E;
    }

    private final static class ReliableSource implements Source<RuntimeException> {

        /** The actual source of the elements. */
        private final Iterator<Integer> source;

        /**
         * Create a new source.
         *
         * @param s
         *            the elements to be returned; it must not be {@code null}
         */
        public ReliableSource(Iterator<Integer> s) {
            source = Objects.requireNonNull(s);
        }

        /**
         * @see cz.ictpro.lectures.java.generics.D02_GenericExceptions.Source#next()
         */
        public Integer next() {
            return source.hasNext() ? source.next() : null;
        }

        /**
         * @see cz.ictpro.lectures.java.generics.D02_GenericExceptions.Source#close()
         */
        public void close() {
            // Nothing to do
        }
    }

    private final static class UnreliableSource implements Source<IOException> {

        /** The wrapped source. */
        private final Source<? extends RuntimeException> source;

        /**
         * Create a new source which makes another source unreliable for this
         * demo.
         *
         * @param s
         *            the source to be wrapped; it must not be {@code null}
         */
        public UnreliableSource(Source<? extends RuntimeException> s) {
            source = Objects.requireNonNull(s);
        }

        /**
         * @see cz.ictpro.lectures.java.generics.D02_GenericExceptions.Source#next()
         */
        public Integer next() throws IOException {
            final Integer result = source.next();
            if (result == 0) { // A coding failure ;o)
                throw new IOException("An error occurred.");
            }

            return result;
        }

        /**
         * @see cz.ictpro.lectures.java.generics.D02_GenericExceptions.Source#close()
         */
        public void close() throws IOException {
            source.close();
        }
    }
}
