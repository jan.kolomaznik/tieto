package cz.mendelu.swi.morse;

import java.io.IOException;
import java.io.Writer;

public class MorseWriter extends Writer {

	private final Writer deroratedWriter;

	private final MorseCode morseCode;

	private boolean isFirstCharInWord = true;

	public MorseWriter(Writer out) {
		super();
		this.deroratedWriter = out;
		this.morseCode = new MorseCode();
	}

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
		for (int i = off; i < len; i++) {
			writeSingleChar(cbuf[i]);
		}
	}

	private void writeSingleChar(char input) throws IOException {
		if (isEndOfSentens(input)) {
			writeSentensSeperator();
		} else if (Character.isAlphabetic(input)) {
			writeAlphabetisChar(input);
		} else if (Character.isWhitespace(input)) {
			writeWordSeperator();
		}
	}

	private boolean isEndOfSentens(char input) {
		return '.' == input;
	}

	private void writeWordSeperator() throws IOException {
		deroratedWriter.write(morseCode.getWorldSeperator());
		isFirstCharInWord = true;
	}

	private void writeAlphabetisChar(char input) throws IOException {
		if (isFirstCharInWord) {
			isFirstCharInWord = false;
		} else {
			deroratedWriter.write(morseCode.getLetterSeperator());
		}
		deroratedWriter.write(morseCode.encode(input));
	}

	private void writeSentensSeperator() throws IOException {
		deroratedWriter.write(morseCode.getSentensSeperator());
		isFirstCharInWord = true;
	}

	@Override
	public void flush() throws IOException {
		deroratedWriter.flush();

	}

	@Override
	public void close() throws IOException {
		deroratedWriter.close();
	}
}
