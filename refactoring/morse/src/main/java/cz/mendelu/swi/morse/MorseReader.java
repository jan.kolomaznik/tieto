package cz.mendelu.swi.morse;

import java.io.IOException;
import java.io.Reader;

public class MorseReader extends Reader {

    private final Reader in;

    private final MorseCode morseCode;

    private int lastChar;

    /**
     * Veřejný konstruktor.
     *
     * @param in vsupem je textový soubor obsahující text v moresově abecedě odělený pomocí svislích lomítek.
     */
    public MorseReader(Reader in) {
        this.in = in;
        this.morseCode = new MorseCode();
        try {
            this.lastChar = in.read();
        } catch (IOException e) {
            throw new IllegalStateException("Can't read first char", e);
        }
    }

    @Override
    public void close() throws IOException {
        in.close();
    }

    /**
     * metoda na čtení
     */
    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int l = -1; // nastavení příznaku pozice před první znak

        // Cyklus který iteruje přes všechny znaky z buferu
        for (int i = off; i < len; i++) {
            int nextChar = -1;
            if (lastChar != -1) {
                if (lastChar == '.' || lastChar == '-') {
                    nextChar = getNextChar();


                } else {
                    lastChar = -1; // nastavení příznaku pozice před první znak

                    // Druh sekvence:
                    // 1. Písmeno
                    // 2. mezera
                    // 3. Konec věty
                    // Vypis podle druhu sekvence
                    switch (getSepatorType()) {
                        case 1:
                            nextChar = getNextChar();
                            break;
                        case 2: // vypis mezery mezi slovy
                            nextChar = ' ';
                            break;
                        case 3: // vypis tečky (konec věty)
                            nextChar = '.';
                            break;
                        default: // Oprava chyby kdy dekoder nepřekládal poslední písmeno
                            int nc5;
                            // Hledání dalšího znaku | (Zkrácená verze aby kód nebyl zbytečně dlouhý)
                            nextChar = getNextChar();
                    }
                }

            }

            // Toto jsme převzal z přednášky, má to ukončit cyklus, když dojdou písmena.
            if (nextChar != -1) {
                cbuf[i] = (char) nextChar;
                l = (l == -1) ? 1 : l + 1;
            } else {
                break;
            }
        }
        return l;
    }

    private int getSepatorType() throws IOException {
        int c = 1;
        int n2c;
        // Přesunuto z cyklu, ošetřena chyba kdy se v textu vyskytovalo více svislých lomítek za sebou
        // Opět cyklus na hledání znaku |
        while ((n2c = in.read()) >= 0) {
            if (n2c == '|') {
                c++;
            } else {
                lastChar = n2c;
                break;
            }
        }
        return c;
    }

    private int getNextChar() throws IOException {
        String letter = readLetter();
        int nextChar = morseCode.decode(letter);
        return nextChar;
    }

    private String readLetter() throws IOException {
        StringBuilder letter = createBuildWithLastChar();
        int nextCodeChar;
        while ((nextCodeChar = in.read()) >= 0) {
            if (nextCodeChar == '|') {
                lastChar = nextCodeChar;
                break;
            } else {
                letter.append((char) nextCodeChar);
            }
        }
        return letter.toString();
    }

    /**
     * Create new StringBuilder with last read character, if it is . or -.
     * @return
     */
    private StringBuilder createBuildWithLastChar() {
        StringBuilder result = new StringBuilder();
        if (lastChar == '.' || lastChar == '-') {
            result.append((char) lastChar);
            lastChar = -1;
        }
        return result;
    }

}
