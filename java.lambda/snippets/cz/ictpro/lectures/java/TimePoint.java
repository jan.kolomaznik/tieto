package cz.ictpro.lectures.java;

import java.util.concurrent.TimeUnit;

/**
 * A utility for measuring elapsed time.
 */
public final class TimePoint {

    /** Origin of the time interval. */
    private final long origin;

    /**
     * Makes a new instance with the starting instant beginning right now.
     */
    public TimePoint() {
        origin = System.nanoTime();
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(TimeUnit.NANOSECONDS);
    }

    /**
     * Gets the formatted time information.
     *
     * @param unit
     *            the unit of the return value. It must not be {@code null}.
     *
     * @return the formatted time information
     */
    public String toString(TimeUnit unit) {
        return String.format("%d %s", elapsed(unit), unit);
    }

    /**
     * Gets the duration from the origin.
     *
     * @param unit
     *            the unit of the return value. It must not be {@code null}.
     *
     * @return the duration from the origin in the given uni
     */
    public long elapsed(TimeUnit unit) {
        return unit.convert(System.nanoTime() - origin, TimeUnit.NANOSECONDS);
    }
}