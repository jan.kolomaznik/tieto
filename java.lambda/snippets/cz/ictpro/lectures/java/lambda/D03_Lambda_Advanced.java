package cz.ictpro.lectures.java.lambda;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

/**
 * Examples of using lambda in a more advanced situations.
 */
public final class D03_Lambda_Advanced {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     *
     * @throws Exception
     *             if something goes wrong
     */
    public static void main(String... args) throws Exception {
        // We already know this: using a method for a lambda body directly
        final ToIntFunction<String> length = String::length;
        System.out.println(length.applyAsInt("string"));

        // But we can use even a constructor! This is how a factory can be made easily.
        final Supplier<Collection<Number>> factory = HashSet::new;

        final Collection<Number> c1 = factory.get();
        c1.add(Math.PI);
        c1.add(1);

        final Collection<Number> c2 = factory.get();
        c2.add(2);

        // These are really diffent
        System.out.println(c1);
        System.out.println(c2);

        // With conversion constructors, it is easy to duplicate or convert objects
        // even without the flawed infamous Cloneable interface
        final Function<Collection<? extends Number>, List<Number>> convertToList = ArrayList<Number>::new;

        final Set<Integer> set = Collections.singleton(42);
        final List<Number> list = convertToList.apply(set);
        list.add(Math.PI);
        System.out.println(set);
        System.out.println(list);

        // Lambda is a polyexpression: the same code is coerced to the context
        final Function<String, Integer> f = String::length;
        System.out.println(f.apply("string"));

        // A lambda can "return" another lambda
        // For the clarity, let's fill the parens around the "returned" lambda
        final Callable<Runnable> callable = () -> (() -> System.out.println("Hello from a Runnable."));
        callable.call().run();

        // For instance a constant function returning a lambda (and now already without the parens as usual)
        final Function<String, Runnable> bind = s -> () -> System.out.println(s);
        final Runnable bound = bind.apply("Hello from a bound Runnable.");
        bound.run();

        // It can be used even for binding arguments in a way
        final Function<String, Consumer<? super String>> hello = s -> t -> System.out.println(s);
        final Function<String, Consumer<? super String>> world = s -> t -> System.out.println(t);
        final Function<String, Consumer<? super String>> whole = s -> t -> System.out.println(s + " " + t);
        hello.apply("Hello").accept("World");
        world.apply("Hello").accept("World");
        whole.apply("Hello").accept("World");
    }
}
