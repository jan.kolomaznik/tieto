package cz.ictpro.lectures.java.lambda;

import java.util.List;

/**
 * Exercise: resolve the tasks in the code below.
 */
public final class E01_CollectionBasics {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        final List<Integer> numbers = null; // TODO: Make a list of random numbers
        // TODO: Remove all even numbers
        // TODO: Sort the list
        // TODO: Print the remaining numbers, one on each line
        // TODO: Do it with lambda and with new methods on the collection
        System.out.println(numbers);
        // TODO: Rewrite all above with a single stream and no intermediate collection
    }
}
